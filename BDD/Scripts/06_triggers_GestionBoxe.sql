-- ====================================================================
-- Script : 06_triggers_GestionBoxe.sql
-- Objet  : Cr�ation des triggers GestionBoxe sur SGBD Oracle en Local 
--
-- Mise � jour des versions
-- Version  Visa   Date      Commentaires
-- -----  ------ --------    ------------------------------------------
-- 1.0	     GA   2.05.08    Cr�ation triggers
-- ====================================================================

-- ====================================================
-- Trigger Droit
-- ====================================================

-- Sequence auto-incr�ments Droit + Trigger --
drop sequence sq_droit;
create sequence sq_droit increment by 1 start with 1 MINVALUE 1 NOMAXVALUE;


create or replace trigger tr_droit
    before insert on droit
    for each row
begin
    select sq_droit.nextval into :NEW.droit_id from dual;
end;

/
ALTER TRIGGER tr_droit ENABLE
/

-- ====================================================
-- Trigger Facture
-- ====================================================

-- Sequence auto-incr�ments Facture + Trigger --
drop sequence sq_facture;
create sequence sq_facture increment by 1 start with 1 MINVALUE 1 NOMAXVALUE;


create or replace trigger tr_facture
    before insert on facture
    for each row
begin
    select sq_facture.nextval into :NEW.facture_id from dual;
end;

/
ALTER TRIGGER tr_facture ENABLE
/
-- ====================================================
-- Trigger Utilisateur
-- ====================================================

-- Sequence auto-incr�ments Utilisteur + Trigger --
drop sequence sq_util;
create sequence sq_util increment by 1 start with 1 MINVALUE 1 NOMAXVALUE;


create or replace trigger tr_util
    before insert on utilisateur
    for each row
begin
    select sq_util.nextval into :NEW.util_id from dual;
end;

/
ALTER TRIGGER tr_util ENABLE
/

-- ====================================================
-- Trigger Horaire
-- ====================================================

-- Sequence auto-incr�ments Horaire + Trigger --
drop sequence sq_horaire;
create sequence sq_horaire increment by 1 start with 1 MINVALUE 1 NOMAXVALUE;


create or replace trigger tr_horaire
    before insert on horaire
    for each row
begin
    select sq_horaire.nextval into :NEW.horaire_id from dual;
end;

/
ALTER TRIGGER tr_horaire ENABLE
/

-- ====================================================
-- Trigger Salle
-- ====================================================

-- Sequence auto-incr�ments Salle + Trigger --
drop sequence sq_salle;
create sequence sq_salle increment by 1 start with 1 MINVALUE 1 NOMAXVALUE;


create or replace trigger tr_salle
    before insert on salle
    for each row
begin
    select sq_salle.nextval into :NEW.salle_id from dual;
end;

/
ALTER TRIGGER tr_salle ENABLE
/

-- ====================================================
-- Trigger Abonnement
-- ====================================================

-- Sequence auto-incr�ments abonnement + Trigger --
drop sequence sq_abo;
create sequence sq_abo increment by 1 start with 1 MINVALUE 1 NOMAXVALUE;


create or replace trigger tr_abo
    before insert on abonnement
    for each row
begin
    select sq_abo.nextval into :NEW.abo_id from dual;
end;

/
ALTER TRIGGER tr_abo ENABLE
/


-- ====================================================
-- Trigger Cours
-- ====================================================

-- Sequence auto-incr�ments Cours + Trigger --
drop sequence sq_cours;
create sequence sq_cours increment by 1 start with 1 MINVALUE 1 NOMAXVALUE;


create or replace trigger tr_cours
    before insert on cours
    for each row
begin
    select sq_cours.nextval into :NEW.cours_id from dual;
end;

/
ALTER TRIGGER tr_cours ENABLE
/








