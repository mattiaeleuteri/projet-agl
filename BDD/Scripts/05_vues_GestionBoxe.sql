-- ======================================================================
--  Script: 05_vues_GestionBoxe.sql 
--  Objet : Cr�ation des vues sch�ma de la base GestionBoxe
--  Objectif : cr�er des vues et tester l'interrogation et la mise-�-jour � travers ces vues. 
-- ======================================================================

-- ====================================================
-- Cr�ation des VUES
-- ====================================================
-- ====================================================
-- Vue d'affichage des factures
-- ====================================================
CREATE OR REPLACE VIEW vw_facture(Numero, Montant, Echeance, Statut) AS
       SELECT facture_id, facture_montant, facture_echeance, facture_statut
       FROM facture
       	 ORDER BY facture_id
;
GRANT SELECT, INSERT, UPDATE, DELETE ON vw_facture TO GestionBoxe_user;

-- ====================================================
-- Vue d'affichage des utilisateurs
-- ====================================================
CREATE OR REPLACE VIEW vw_util(Numero, Nom, Prenom, Telephone, Email, Abonnement) AS
       SELECT util_id, util_nom, util_prenom, util_tel, util_email, abo_nom
       FROM utilisateur
       INNER JOIN abonnement ON abonnement.abo_id = utilisateur.abo_id
         ORDER BY util_nom
;
GRANT SELECT, INSERT, UPDATE, DELETE ON vw_util TO GestionBoxe_user;

-- ====================================================
-- Vue d'affichage de la liste des abonnements
-- ====================================================
CREATE OR REPLACE VIEW vw_abo(Numero, Nom, Prix, Duree) AS
       SELECT abo_id, abo_nom, abo_prix, abo_duree
       FROM abonnement
         ORDER BY abo_nom
;
GRANT SELECT, INSERT, UPDATE, DELETE ON vw_abo TO GestionBoxe_user;

-- ====================================================
-- Vue d'affichage de la liste des cours
-- ====================================================
CREATE OR REPLACE VIEW vw_cours(Numero, Nom, Salle, Jour, Debut, Fin) AS
       SELECT se_d�roule.cours_id, cours_nom, salle_id, cours_jour, cours_debut, cours_fin
       FROM se_d�roule
       INNER JOIN cours ON se_d�roule.cours_id = cours.cours_id
         ORDER BY se_d�roule.cours_id
;
GRANT SELECT, INSERT, UPDATE, DELETE ON vw_cours TO GestionBoxe_user;
  
