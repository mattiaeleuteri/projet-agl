-- ====================================================================
-- Script : 07_package_GestionBoxe.sql
-- Objet  : Création d'un package GestionBoxe sur SGBD Oracle en Local 
--
-- Mise à jour des versions
-- Version  Visa   Date      Commentaires
-- -----  ------ --------    ------------------------------------------
-- 1.0	     GA   2.05.08    Création package
-- ====================================================================

create or replace package pkg_statsAbo
AS
       FUNCTION CountAbo RETURN NUMBER;         
END pkg_statsAbo;
/

create or replace package body pkg_statsAbo
AS

FUNCTION CountAbo RETURN NUMBER
IS
         v_nb_abo NUMBER;
BEGIN
         SELECT COUNT(*) INTO v_nb_abo FROM abonnement;
	 RETURN v_nb_abo;
END;

END;
/
GRANT EXECUTE ON pkg_statsAbo TO role_GestionBoxe_user;