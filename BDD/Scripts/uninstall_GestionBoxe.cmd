REM Fichier		: BDD_GestionBoxe.cmd
REM Objet		: Désinstallation de la BDD GestionBoxe

mkdir .\Logs

REM Lancement du script uninstall_GestionBoxe.sql dans SQL*PLUS
REM le %1 permet de demander le mot de passe lors du lancement du script
sqlplus SYSTEM/manager@XE @%~dp0\uninstall_GestionBoxe.sql @%~dp0