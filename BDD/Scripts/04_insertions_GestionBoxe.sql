-- ====================================================================
-- Script : 04_insertions_GestionBoxe.sql
-- Objet  : Insertions de donn�es sur les tables GestionBoxe
-- ====================================================================
-- ====================================================
-- Insertion Table : droit
-- ====================================================
INSERT INTO droit VALUES (1, 1);
INSERT INTO droit VALUES (2, 2);
INSERT INTO droit VALUES (3, 3);

COMMIT;
-- ====================================================
-- Insertion Table : facture
-- ====================================================
insert into facture values (null, '42.92', '13/04/2020', 'open');
insert into facture values (null, '46.95', '16/08/2019', 'open');
insert into facture values (null, '34.10', '13/12/2019', 'open');
insert into facture values (null, '12.33', '17/10/2019', 'open');
insert into facture values (null, '13.03', '27/06/2019', 'closed');
insert into facture values (null, '24.75', '10/01/2020', 'closed');
insert into facture values (null, '43.57', '29/01/2020', 'open');
insert into facture values (null, '45.78', '01/03/2020', 'open');
insert into facture values (null, '13.65', '16/10/2019', 'open');
insert into facture values (null, '30.97', '02/06/2019', 'open');
insert into facture values (null, '22.00', '18/02/2020', 'closed');
insert into facture values (null, '10.01', '03/12/2019', 'closed');
insert into facture values (null, '39.23', '18/11/2019', 'closed');
insert into facture values (null, '28.96', '03/07/2019', 'open');
insert into facture values (null, '43.66', '07/09/2019', 'open');
insert into facture values (null, '11.97', '17/08/2019', 'open');
insert into facture values (null, '47.84', '28/07/2019', 'open');
insert into facture values (null, '35.40', '18/04/2020', 'open');
insert into facture values (null, '20.04', '30/01/2020', 'open');
insert into facture values (null, '14.44', '12/10/2019', 'open');
insert into facture values (null, '47.72', '01/08/2019', 'open');
insert into facture values (null, '12.63', '01/01/2020', 'closed');
insert into facture values (null, '38.75', '09/12/2019', 'open');
insert into facture values (null, '2.32', '15/04/2020', 'open');

COMMIT;
-- ====================================================
-- Insertion Table : utilisateur
-- ====================================================
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Sundin', 'Hatti', '973-131-7151', 'hsundin0@ustream.tv', '7QTT1hV3L');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Kundert', 'Hettie', '232-714-1542', 'hkundert1@quantcast.com', '8bodenEMv4');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Halcro', 'Michelle', '239-881-8308', 'mhalcro2@princeton.edu', '6Kw4h9');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Hanscom', 'Papagena', '248-134-2100', 'phanscom3@addtoany.com', 'ArEtGr');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Duchart', 'Stu', '513-111-9715', 'sduchart4@upenn.edu', 'c8mRVn');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Impy', 'Evan', '785-248-0742', 'eimpy5@newyorker.com', 'v2fkkJUjLG');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'D''Emanuele', 'Bil', '315-800-2890', 'bdemanuele6@eepurl.com', 'ktGIKYSE5');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Gale', 'Alleen', '118-371-3392', 'agale7@fc2.com', '7nNkYGwmCUV');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Steffan', 'Zelig', '583-914-0000', 'zsteffan8@cbslocal.com', '0BBHvD');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Laker', 'Franni', '727-148-4439', 'flaker9@usgs.gov', 'swI8q1YBz');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Kondratowicz', 'Jeanette', '282-692-6826', 'jkondratowicza@joomla.org', 'kstFy7ZpBTV6');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Clew', 'Valentine', '698-995-5576', 'vclewb@microsoft.com', 'ikaB9zW');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Gerhardt', 'Zonda', '707-841-6132', 'zgerhardtc@joomla.org', 'w9SYfrHR');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Mongan', 'Dugald', '225-769-1898', 'dmongand@cloudflare.com', 'a4rNocAk');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Crumley', 'Angil', '967-562-9926', 'acrumleye@parallels.com', 'TiF3cYY');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Lukesch', 'Jacquelyn', '931-532-0495', 'jlukeschf@si.edu', 'c8siov0B1n');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Poltone', 'Kathye', '161-547-7010', 'kpoltoneg@shinystat.com', '2FoYkXeDQgRf');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Slany', 'Westleigh', '996-982-2977', 'wslanyh@unicef.org', 'keBEjkDuI');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'de Lloyd', 'Micah', '573-618-3576', 'mdelloydi@dedecms.com', 'wtLb4BaR2');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Matskiv', 'Berty', '689-108-0581', 'bmatskivj@paginegialle.it', 'Lk4rkgcB1p');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 3, 'Eleuteri', 'Mattia', '0798547179', 'admin', 'admin');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 1, 'Eleuteri', 'Mattia', '0798547179', 'membre', 'membre');
insert into utilisateur (util_id, abo_id, droit_id, util_nom, util_prenom, util_tel, util_email, util_mdp) values (null, null, 2, 'Eleuteri', 'Mattia', '0798547179', 'coach', 'coach');

COMMIT;
-- ====================================================
-- Insertion Table : horaire
-- ====================================================
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '14/03/2020', '04/01/2020');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '19/10/2019', '10/04/2020');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '13/04/2020', '25/01/2020');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '18/05/2019', '20/04/2020');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '15/01/2020', '15/12/2019');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '29/03/2020', '27/12/2019');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '15/03/2020', '08/02/2020');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '16/10/2019', '27/08/2019');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '18/06/2019', '07/10/2019');
insert into horaire (horaire_id, horaire_occup�, horaire_jour) values (null, '24/09/2019', '09/12/2019');

COMMIT;
-- ====================================================
-- Insertion Table : salle
-- ====================================================
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);
insert into salle (salle_id) values (null);

COMMIT;
-- ====================================================
-- Insertion Table : abonnement
-- ====================================================
insert into abonnement (abo_id, facture_id, abo_nom, abo_prix, abo_duree) values (null, 17, 'Lindheimera texana A. Gray Engelm', '26.65', 12);
insert into abonnement (abo_id, facture_id, abo_nom, abo_prix, abo_duree) values (null, 24, 'Oenothera pilosella Raf', '39.76', 11);
update utilisateur set abo_id = 1 WHERE util_id = 1;
update utilisateur set abo_id = 2 WHERE util_id = 2;
update utilisateur set abo_id = 1 WHERE util_id = 3;
update utilisateur set abo_id = 1 WHERE util_id = 4;
update utilisateur set abo_id = 2 WHERE util_id = 5;
update utilisateur set abo_id = 1 WHERE util_id = 6;
update utilisateur set abo_id = 1 WHERE util_id = 7;
update utilisateur set abo_id = 1 WHERE util_id = 8;
update utilisateur set abo_id = 2 WHERE util_id = 9;
update utilisateur set abo_id = 1 WHERE util_id = 10;
update utilisateur set abo_id = 1 WHERE util_id = 11;
update utilisateur set abo_id = 1 WHERE util_id = 12;
update utilisateur set abo_id = 1 WHERE util_id = 13;
update utilisateur set abo_id = 2 WHERE util_id = 14;
update utilisateur set abo_id = 2 WHERE util_id = 15;
update utilisateur set abo_id = 2 WHERE util_id = 16;
update utilisateur set abo_id = 1 WHERE util_id = 17;
update utilisateur set abo_id = 2 WHERE util_id = 18;
update utilisateur set abo_id = 1 WHERE util_id = 19;
update utilisateur set abo_id = 2 WHERE util_id = 20;
update utilisateur set abo_id = 2 WHERE util_id = 22;
update utilisateur set abo_id = 1 WHERE util_id = 23;


COMMIT;
-- ====================================================
-- Insertion Table : cours
-- ====================================================
insert into cours (cours_id, cours_nom) values (null, 'Cape raven');
insert into cours (cours_id, cours_nom) values (null, 'Blue racer');
insert into cours (cours_id, cours_nom) values (null, 'Butterfly, tropical buckeye');
insert into cours (cours_id, cours_nom) values (null, 'White-bellied sea eagle');
insert into cours (cours_id, cours_nom) values (null, 'Yellow baboon');
insert into cours (cours_id, cours_nom) values (null, 'Bush dog');
insert into cours (cours_id, cours_nom) values (null, 'Genet, small-spotted');
insert into cours (cours_id, cours_nom) values (null, 'Antechinus, brown');
insert into cours (cours_id, cours_nom) values (null, 'Owl, burrowing');
insert into cours (cours_id, cours_nom) values (null, 'Flamingo, roseat');
insert into cours (cours_id, cours_nom) values (null, 'Emerald green tree boa');
insert into cours (cours_id, cours_nom) values (null, 'Lynx, african');
insert into cours (cours_id, cours_nom) values (null, 'Mexican boa');
insert into cours (cours_id, cours_nom) values (null, 'Tortoise, desert');
insert into cours (cours_id, cours_nom) values (null, 'Waved albatross');
insert into cours (cours_id, cours_nom) values (null, 'Vulture, white-headed');
insert into cours (cours_id, cours_nom) values (null, 'Porcupine, north american');
insert into cours (cours_id, cours_nom) values (null, 'Brazilian tapir');
insert into cours (cours_id, cours_nom) values (null, 'Wolf, common');
insert into cours (cours_id, cours_nom) values (null, 'Striated heron');

COMMIT;
-- ====================================================
-- Insertion Table : participe
-- ====================================================
insert into participe (util_id, cours_id) values (10, 7);
insert into participe (util_id, cours_id) values (6, 9);
insert into participe (util_id, cours_id) values (20, 5);
insert into participe (util_id, cours_id) values (5, 8);
insert into participe (util_id, cours_id) values (13, 3);
insert into participe (util_id, cours_id) values (20, 6);
insert into participe (util_id, cours_id) values (20, 16);
insert into participe (util_id, cours_id) values (7, 18);
insert into participe (util_id, cours_id) values (8, 8);
insert into participe (util_id, cours_id) values (3, 8);
insert into participe (util_id, cours_id) values (20, 8);
insert into participe (util_id, cours_id) values (18, 10);

COMMIT;
-- ====================================================
-- Insertion Table : poss�de
-- ====================================================
insert into poss�de (salle_id, horaire_id) values (7, 8);
insert into poss�de (salle_id, horaire_id) values (9, 7);
insert into poss�de (salle_id, horaire_id) values (4, 3);
insert into poss�de (salle_id, horaire_id) values (5, 7);
insert into poss�de (salle_id, horaire_id) values (3, 2);
insert into poss�de (salle_id, horaire_id) values (9, 9);
insert into poss�de (salle_id, horaire_id) values (4, 4);
insert into poss�de (salle_id, horaire_id) values (3, 7);
insert into poss�de (salle_id, horaire_id) values (10, 1);
insert into poss�de (salle_id, horaire_id) values (4, 5);
insert into poss�de (salle_id, horaire_id) values (8, 2);
insert into poss�de (salle_id, horaire_id) values (4, 2);

COMMIT;
-- ====================================================
-- Insertion Table : se_d�roule
-- ====================================================
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (2, 4, '19/03/2020', '28/02/2020', '23/11/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (16, 8, '09/10/2019', '19/04/2020', '08/10/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (14, 10, '05/05/2019', '25/01/2020', '27/04/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (10, 8, '07/06/2019', '09/04/2020', '11/04/2020');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (4, 7, '09/02/2020', '04/01/2020', '21/11/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (20, 7, '18/12/2019', '15/03/2020', '24/10/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (5, 4, '30/05/2019', '01/07/2019', '17/08/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (13, 3, '04/12/2019', '16/08/2019', '29/05/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (12, 7, '31/03/2020', '03/10/2019', '30/01/2020');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (9, 4, '26/06/2019', '09/03/2020', '20/11/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (11, 3, '02/02/2020', '06/10/2019', '05/12/2019');
insert into se_d�roule (cours_id, salle_id, cours_debut, cours_fin, cours_jour) values (5, 2, '28/01/2020', '08/03/2020', '19/01/2020');

COMMIT;














