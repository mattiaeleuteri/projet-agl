-- ======================================================================
--  Script: 07_synonymes_GestionBoxe.sql 
--  Objet : Cr�ation des synonymes de la base GestionBoxe
-- ======================================================================

CREATE OR REPLACE SYNONYM GestionBoxe_user.vw_facture FOR GestionBoxe_data.vw_facture;
CREATE OR REPLACE SYNONYM GestionBoxe_user.vw_util FOR GestionBoxe_data.vw_util;
CREATE OR REPLACE SYNONYM GestionBoxe_user.vw_abo FOR GestionBoxe_data.vw_abo;
CREATE OR REPLACE SYNONYM GestionBoxe_user.vw_cours FOR GestionBoxe_data.vw_cours;
CREATE OR REPLACE SYNONYM GestionBoxe_user.pkg_statsAbo FOR GestionBoxe_data.pkg_statsAbo;

CREATE OR REPLACE SYNONYM GestionBoxe_user.sq_droit FOR GestionBoxe_data.sq_droit;
CREATE OR REPLACE SYNONYM GestionBoxe_user.sq_facture FOR GestionBoxe_data.sq_facture;
CREATE OR REPLACE SYNONYM GestionBoxe_user.sq_util FOR GestionBoxe_data.sq_util;
CREATE OR REPLACE SYNONYM GestionBoxe_user.sq_horaire FOR GestionBoxe_data.sq_horaire;
CREATE OR REPLACE SYNONYM GestionBoxe_user.sq_salle FOR GestionBoxe_data.sq_salle;
CREATE OR REPLACE SYNONYM GestionBoxe_user.sq_abo FOR GestionBoxe_data.sq_abo;
CREATE OR REPLACE SYNONYM GestionBoxe_user.sq_cours FOR GestionBoxe_data.sq_cours;