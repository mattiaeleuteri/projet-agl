-- ====================================================================
-- Script : 02_utilisateurs_GestionBoxe.sql
-- Objet  : Cr�ation des utilisateurs BDD GestionBoxe sur SGBD Oracle en Local (XE)
--
-- Mise � jour des versions
-- Version  Visa   Date      Commentaires
-- -----  ------ --------    ------------------------------------------
-- 1.0	     GA   26.03.07    Cr�ation utilisateurs
-- ====================================================================
-- =============================================================================================
-- Suppression des r�les, utilisateurs et profil
-- =============================================================================================
-- Suppression des utilisateurs
DROP USER GestionBoxe_data CASCADE;
DROP USER GestionBoxe_user CASCADE;

-- Suppression des r�les
DROP ROLE role_GestionBoxe_data CASCADE;
DROP ROLE role_GestionBoxe_user CASCADE;

-- Suppression du profil
DROP PROFILE GestionBoxe_profil;

-- =============================================================================================
-- Cr�ation du profil
-- =============================================================================================
-- Creation du profil GestionBoxe_profil
CREATE PROFILE GestionBoxe_profil LIMIT
	SESSIONS_PER_USER 3
	FAILED_LOGIN_ATTEMPTS 3 
	PASSWORD_LOCK_TIME 1/24
	PASSWORD_LIFE_TIME 180 
	PASSWORD_REUSE_TIME 180 
	PASSWORD_REUSE_MAX UNLIMITED
	PASSWORD_GRACE_TIME 7;

-- =============================================================================================
-- Cr�ation du r�le role_GestionBoxe_data (r�le de l'utilisateur propri�taire des objets de la base)
-- =============================================================================================
-- Creation du r�le role_GestionBoxe_data
CREATE ROLE role_GestionBoxe_data;

-- droit de se connecter � la BDD
GRANT CONNECT TO role_GestionBoxe_data;

-- droit de cr�er des triggers, s�quence, tables, packages, ...
GRANT RESOURCE TO role_GestionBoxe_data;

-- droit de cr�er des vues
GRANT CREATE VIEW TO role_GestionBoxe_data;

-- droit de cr�er des synonymes
-- GRANT CREATE ANY SYNONYM TO role_GestionBoxe_data;
-- ! fonctionne mais donne trop de privil�ges, � �viter

-- ===============================
-- Cr�ation du r�le role_GestionBoxe_user (r�le de l'utilisateur de l'application)
-- ===============================
-- Creation du r�le role_GestionBoxe_user
CREATE ROLE role_GestionBoxe_user;

-- droit de se connecter � la BDD
GRANT CREATE SESSION TO role_GestionBoxe_user;

-- ===================================
-- Cr�ation de l'utilisateur GestionBoxe_data (propri�taire des objets de sch�ma de la base)
-- ===================================
-- Creation de l'utilisateur GestionBoxe_data 
CREATE USER GestionBoxe_data
	PROFILE GestionBoxe_profil
	IDENTIFIED BY GestionBoxe_data
	DEFAULT TABLESPACE USERS
	TEMPORARY TABLESPACE TEMP
;
GRANT role_GestionBoxe_data TO GestionBoxe_data;
ALTER USER GestionBoxe_data quota unlimited ON USERS;

--===================================
--Cr�ation de l'utilisateur GestionBoxe_user (utilisateur de l'application)
--===================================
CREATE USER GestionBoxe_user
	PROFILE GestionBoxe_profil
	IDENTIFIED BY GestionBoxe_user
;
GRANT role_GestionBoxe_user TO GestionBoxe_user;
