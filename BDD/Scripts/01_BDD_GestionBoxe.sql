-- ====================================================================
-- Script : 01_BDD_GestionBoxe.sql
-- Objet  : Cr�ation BDD GestionBoxe sur SGBD Oracle en Local (XE) 
--
-- Mise � jour des versions
-- Version  Visa   Date      Commentaires
-- -----  ------ --------    ---------------------------------------------
-- 1.0	     GA   2.05.08    Cr�ation
-- ====================================================================

SET ECHO ON
SET LINESIZE 200
SET PAGESIZE 60

-- Cr�ation des utilisateurs en tant que System
SPOOL .\Logs\02_utilisateurs_GestionBoxe.log
@.\02_utilisateurs_GestionBoxe.sql
SPOOL OFF

-- Connexion � HEGLOCAL en tant que EasyCooking_data (propir�taires des objets de sch�ma)
CONNECT GestionBoxe_data/GestionBoxe_data@XE

-- Cr�ation des tables
SPOOL .\Logs\03_tables_GestionBoxe.log
@.\03_tables_GestionBoxe.sql
SPOOL OFF

-- Cr�ation des triggers pour les clefs primaires
SPOOL .\Logs\06_triggersGestionBoxe.log
@.\06_triggers_GestionBoxe.sql;
SPOOL OFF

-- Chargement des donn�es
SPOOL .\Logs\04_insertions_GestionBoxe.log
@.\04_insertions_GestionBoxe.sql;
SPOOL OFF

-- Cr�ation des vues
SPOOL .\Logs\05_vues_GestionBoxe.log
@.\05_vues_GestionBoxe.sql
SPOOL OFF

-- Cr�ation des packages
SPOOL .\Logs\07_package_GestionBoxe.log
@.\07_package_GestionBoxe.sql;
SPOOL OFF

-- Connexion � HEGLOCAL en tant que system
CONNECT system/manager@XE

-- Cr�ation des synonymes
SPOOL .\Logs\08_synonymes_GestionBoxe.log
@.\08_synonymes_GestionBoxe.sql;
SPOOL OFF

SET ECHO OFF

EXIT;
