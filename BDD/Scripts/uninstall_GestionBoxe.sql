--------------------------------------------------------------------------------------------
--Script : uninstall_GestionBoxe.sql
--Objet  : Cr�ation des synonymes de la BDD d�monstration sur SGBD Oracle en Local
--
--Mise � jour des versions
--Version  Visa   Date      Commentaires
-------  ------ --------    ----------------------------------------------------------------
--1.0	     SC   26.03.07    Cr�ation
--------------------------------------------------------------------------------------------

-- Suppression des utilisateurs (CASCADE), r�les et profil
SPOOL .\Logs\uninstall_GestionBoxe.log

--=============================================
--Suppression des r�les, utilisateurs et profil
--=============================================
--Suppression des utilisateurs
DROP USER GestionBoxe_data CASCADE;
DROP USER GestionBoxe_user CASCADE;

--Suppression des r�les
DROP ROLE role_GestionBoxe_data CASCADE;
DROP ROLE role_GestionBoxe_user CASCADE;

--Suppression du profil
DROP PROFILE GestionBoxe_profil;

SPOOL OFF

EXIT;