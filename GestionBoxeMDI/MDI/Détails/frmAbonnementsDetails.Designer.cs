﻿namespace GestionBoxeMDI
{
    partial class frmAbonnementsDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuit = new System.Windows.Forms.Button();
            this.btnInscriptionDesinscription = new System.Windows.Forms.Button();
            this.txtDescriptionAbo = new System.Windows.Forms.TextBox();
            this.lblDescriptionAbo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.Location = new System.Drawing.Point(114, 227);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(188, 23);
            this.btnQuit.TabIndex = 3;
            this.btnQuit.Text = "Retour à la liste des abonnements";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // btnInscriptionDesinscription
            // 
            this.btnInscriptionDesinscription.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnInscriptionDesinscription.Location = new System.Drawing.Point(114, 198);
            this.btnInscriptionDesinscription.Name = "btnInscriptionDesinscription";
            this.btnInscriptionDesinscription.Size = new System.Drawing.Size(188, 23);
            this.btnInscriptionDesinscription.TabIndex = 2;
            this.btnInscriptionDesinscription.Text = "S\'inscrire / Se désinscrire";
            this.btnInscriptionDesinscription.UseVisualStyleBackColor = true;
            // 
            // txtDescriptionAbo
            // 
            this.txtDescriptionAbo.Location = new System.Drawing.Point(12, 28);
            this.txtDescriptionAbo.Multiline = true;
            this.txtDescriptionAbo.Name = "txtDescriptionAbo";
            this.txtDescriptionAbo.ReadOnly = true;
            this.txtDescriptionAbo.Size = new System.Drawing.Size(393, 146);
            this.txtDescriptionAbo.TabIndex = 1;
            // 
            // lblDescriptionAbo
            // 
            this.lblDescriptionAbo.AutoSize = true;
            this.lblDescriptionAbo.Location = new System.Drawing.Point(12, 9);
            this.lblDescriptionAbo.Name = "lblDescriptionAbo";
            this.lblDescriptionAbo.Size = new System.Drawing.Size(141, 13);
            this.lblDescriptionAbo.TabIndex = 0;
            this.lblDescriptionAbo.Text = "Description de l\'abonnement";
            // 
            // frmAbonnementsDetails
            // 
            this.AcceptButton = this.btnInscriptionDesinscription;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnQuit;
            this.ClientSize = new System.Drawing.Size(417, 267);
            this.ControlBox = false;
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnInscriptionDesinscription);
            this.Controls.Add(this.txtDescriptionAbo);
            this.Controls.Add(this.lblDescriptionAbo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximumSize = new System.Drawing.Size(433, 306);
            this.MinimumSize = new System.Drawing.Size(433, 306);
            this.Name = "frmAbonnementsDetails";
            this.Text = "Détails de l\'abonnement x";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button btnInscriptionDesinscription;
        private System.Windows.Forms.TextBox txtDescriptionAbo;
        private System.Windows.Forms.Label lblDescriptionAbo;
    }
}