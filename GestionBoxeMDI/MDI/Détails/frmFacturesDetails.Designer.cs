﻿namespace GestionBoxeMDI
{
    partial class frmFacturesDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuit = new System.Windows.Forms.Button();
            this.btnPayer = new System.Windows.Forms.Button();
            this.txtDescriptionFacture = new System.Windows.Forms.TextBox();
            this.lblDescriptionFacture = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.Location = new System.Drawing.Point(124, 227);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(169, 23);
            this.btnQuit.TabIndex = 3;
            this.btnQuit.Text = "Retour à la liste des factures";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // btnPayer
            // 
            this.btnPayer.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPayer.Location = new System.Drawing.Point(124, 198);
            this.btnPayer.Name = "btnPayer";
            this.btnPayer.Size = new System.Drawing.Size(169, 23);
            this.btnPayer.TabIndex = 2;
            this.btnPayer.Text = "Payer";
            this.btnPayer.UseVisualStyleBackColor = true;
            // 
            // txtDescriptionFacture
            // 
            this.txtDescriptionFacture.Location = new System.Drawing.Point(12, 28);
            this.txtDescriptionFacture.Multiline = true;
            this.txtDescriptionFacture.Name = "txtDescriptionFacture";
            this.txtDescriptionFacture.ReadOnly = true;
            this.txtDescriptionFacture.Size = new System.Drawing.Size(393, 146);
            this.txtDescriptionFacture.TabIndex = 1;
            // 
            // lblDescriptionFacture
            // 
            this.lblDescriptionFacture.AutoSize = true;
            this.lblDescriptionFacture.Location = new System.Drawing.Point(12, 9);
            this.lblDescriptionFacture.Name = "lblDescriptionFacture";
            this.lblDescriptionFacture.Size = new System.Drawing.Size(122, 13);
            this.lblDescriptionFacture.TabIndex = 0;
            this.lblDescriptionFacture.Text = "Description de la facture";
            // 
            // frmFacturesDetails
            // 
            this.AcceptButton = this.btnPayer;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnQuit;
            this.ClientSize = new System.Drawing.Size(417, 267);
            this.ControlBox = false;
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnPayer);
            this.Controls.Add(this.txtDescriptionFacture);
            this.Controls.Add(this.lblDescriptionFacture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximumSize = new System.Drawing.Size(433, 306);
            this.MinimumSize = new System.Drawing.Size(433, 306);
            this.Name = "frmFacturesDetails";
            this.Text = "Détails de la facture x";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button btnPayer;
        private System.Windows.Forms.TextBox txtDescriptionFacture;
        private System.Windows.Forms.Label lblDescriptionFacture;
    }
}