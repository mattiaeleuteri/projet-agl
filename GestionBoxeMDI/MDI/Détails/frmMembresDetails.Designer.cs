﻿namespace GestionBoxeMDI
{
    partial class frmMembresDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuit = new System.Windows.Forms.Button();
            this.txtDetailsMembre = new System.Windows.Forms.TextBox();
            this.lblDetailsMembre = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.Location = new System.Drawing.Point(124, 199);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(169, 23);
            this.btnQuit.TabIndex = 2;
            this.btnQuit.Text = "Retour à la liste des membres";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // txtDetailsMembre
            // 
            this.txtDetailsMembre.Location = new System.Drawing.Point(12, 28);
            this.txtDetailsMembre.Multiline = true;
            this.txtDetailsMembre.Name = "txtDetailsMembre";
            this.txtDetailsMembre.ReadOnly = true;
            this.txtDetailsMembre.Size = new System.Drawing.Size(393, 146);
            this.txtDetailsMembre.TabIndex = 1;
            // 
            // lblDetailsMembre
            // 
            this.lblDetailsMembre.AutoSize = true;
            this.lblDetailsMembre.Location = new System.Drawing.Point(12, 9);
            this.lblDetailsMembre.Name = "lblDetailsMembre";
            this.lblDetailsMembre.Size = new System.Drawing.Size(94, 13);
            this.lblDetailsMembre.TabIndex = 0;
            this.lblDetailsMembre.Text = "Détails du membre";
            // 
            // frmMembresDetails
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnQuit;
            this.ClientSize = new System.Drawing.Size(417, 237);
            this.ControlBox = false;
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.txtDetailsMembre);
            this.Controls.Add(this.lblDetailsMembre);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximumSize = new System.Drawing.Size(433, 276);
            this.MinimumSize = new System.Drawing.Size(433, 276);
            this.Name = "frmMembresDetails";
            this.Text = "Détails membre x";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.TextBox txtDetailsMembre;
        private System.Windows.Forms.Label lblDetailsMembre;
    }
}