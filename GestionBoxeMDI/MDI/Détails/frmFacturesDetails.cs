﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmFacturesDetails : Form
    {
        public frmFacturesDetails()
        {
            InitializeComponent();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
