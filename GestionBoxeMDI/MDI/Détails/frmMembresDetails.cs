﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmMembresDetails : Form
    {
        public frmMembresDetails()
        {
            InitializeComponent();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
