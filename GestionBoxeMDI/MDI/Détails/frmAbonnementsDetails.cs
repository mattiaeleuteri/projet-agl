﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmAbonnementsDetails : Form
    {
        public frmAbonnementsDetails()
        {
            InitializeComponent();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
