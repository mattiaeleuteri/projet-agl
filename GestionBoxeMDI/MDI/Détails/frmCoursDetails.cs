﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmCoursDetails : Form
    {
        public frmCoursDetails()
        {
            InitializeComponent();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
