﻿namespace GestionBoxeMDI
{
    partial class frmCoursDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescriptionCours = new System.Windows.Forms.Label();
            this.txtDescriptionCours = new System.Windows.Forms.TextBox();
            this.btnInscriptionDesinscription = new System.Windows.Forms.Button();
            this.btnQuit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblDescriptionCours
            // 
            this.lblDescriptionCours.AutoSize = true;
            this.lblDescriptionCours.Location = new System.Drawing.Point(12, 9);
            this.lblDescriptionCours.Name = "lblDescriptionCours";
            this.lblDescriptionCours.Size = new System.Drawing.Size(104, 13);
            this.lblDescriptionCours.TabIndex = 0;
            this.lblDescriptionCours.Text = "Description du cours";
            // 
            // txtDescriptionCours
            // 
            this.txtDescriptionCours.Location = new System.Drawing.Point(12, 28);
            this.txtDescriptionCours.Multiline = true;
            this.txtDescriptionCours.Name = "txtDescriptionCours";
            this.txtDescriptionCours.ReadOnly = true;
            this.txtDescriptionCours.Size = new System.Drawing.Size(393, 146);
            this.txtDescriptionCours.TabIndex = 1;
            // 
            // btnInscriptionDesinscription
            // 
            this.btnInscriptionDesinscription.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnInscriptionDesinscription.Location = new System.Drawing.Point(124, 198);
            this.btnInscriptionDesinscription.Name = "btnInscriptionDesinscription";
            this.btnInscriptionDesinscription.Size = new System.Drawing.Size(169, 23);
            this.btnInscriptionDesinscription.TabIndex = 2;
            this.btnInscriptionDesinscription.Text = "S\'inscrire / Se désinscrire";
            this.btnInscriptionDesinscription.UseVisualStyleBackColor = true;
            // 
            // btnQuit
            // 
            this.btnQuit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuit.Location = new System.Drawing.Point(124, 227);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(169, 23);
            this.btnQuit.TabIndex = 3;
            this.btnQuit.Text = "Retour à la liste des cours";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // frmCoursDetails
            // 
            this.AcceptButton = this.btnInscriptionDesinscription;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnQuit;
            this.ClientSize = new System.Drawing.Size(417, 267);
            this.ControlBox = false;
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnInscriptionDesinscription);
            this.Controls.Add(this.txtDescriptionCours);
            this.Controls.Add(this.lblDescriptionCours);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximumSize = new System.Drawing.Size(433, 306);
            this.MinimumSize = new System.Drawing.Size(433, 306);
            this.Name = "frmCoursDetails";
            this.Text = "Détails du cours x";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDescriptionCours;
        private System.Windows.Forms.TextBox txtDescriptionCours;
        private System.Windows.Forms.Button btnInscriptionDesinscription;
        private System.Windows.Forms.Button btnQuit;
    }
}