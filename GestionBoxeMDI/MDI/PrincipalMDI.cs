﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class PrincipalMDI : Form
    {

        static private frmCours frmCours;
        static private frmAbonnements frmAbo;
        static private frmFactures frmFac;
        static private frmMembres frmMembres;

        public PrincipalMDI()
        {
            InitializeComponent();
        }

        private void PrincipalMDI_Shown(object sender, EventArgs e)
        {
            frmLogin F = new frmLogin();
            F.ShowDialog();
            ssrLblNomUtilisateur.Text = Session.prenom + ' ' + Session.nom;
            if (Session.droitId != 3)
            {
                tsmMDIMembres.Visible = false;
            }
        }

        private void fermerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listeDesCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmCours is null || frmCours.IsDisposed)
            {
                frmCours = new frmCours();
            }
            frmCours.MdiParent = this;
            frmCours.Show();
            frmCours.Focus();
        }

        private void vosFacturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmFac is null || frmFac.IsDisposed)
            {
                frmFac = new frmFactures();
            }
            frmFac.MdiParent = this;
            frmFac.Show();
            frmFac.Focus();

        }

        private void vosAbonnementsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmAbo is null || frmAbo.IsDisposed)
            {
                frmAbo = new frmAbonnements();
            }
            frmAbo.MdiParent = this;
            frmAbo.Show();
            frmAbo.Focus();

        }

        private void listeDesMembresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmMembres is null || frmMembres.IsDisposed)
            {
                frmMembres = new frmMembres();
            }
            frmMembres.MdiParent = this;
            frmMembres.Show();
            frmMembres.Focus();

        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void ssrLblNomUtilisateur_Click(object sender, EventArgs e)
        {

        }

        private void tsmMDIApplication_Click(object sender, EventArgs e)
        {

        }
    }
}
