﻿using GestionBoxeMDI.tablesTableAdapters;
using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmAjoutAbo : Form
    {
        public frmAjoutAbo()
        {
            InitializeComponent();
        }

        private void frmAjoutAbo_Load(object sender, EventArgs e)
        {
            cboDuree.SelectedIndex = 0;
        }

        private void btnAjout_Click(object sender, EventArgs e)
        {
            ABONNEMENTTableAdapter abos = new ABONNEMENTTableAdapter();
            string nom = txtNom.Text;
            decimal prix;
            if (Decimal.TryParse(txtPrix.Text, out prix)) { 
                prix = Convert.ToDecimal(txtPrix.Text); 
            } else
            {
                MessageBox.Show("Le prix indiqué n'est pas bon", "Mauvais prix", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            int duree = cboDuree.SelectedIndex == 0 ? 6 : 12;
            if (nom == "") {
                MessageBox.Show("Veuillez remplir tous les champs !", "Champ vide", MessageBoxButtons.OK, MessageBoxIcon.Error);

            } else
            {
                abos.InsertAbo(nom, prix, duree);
                MessageBox.Show("Abonnement crée et ajouté à la base de donnée !", "Abonnement ajouté", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }
    }
}
