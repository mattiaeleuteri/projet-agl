﻿using GestionBoxeMDI.tablesTableAdapters;
using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmAjoutMembre : Form
    {
        public frmAjoutMembre()
        {
            InitializeComponent();
        }

        private void btnAjout_Click(object sender, EventArgs e)
        {
            UTILISATEURTableAdapter utils = new UTILISATEURTableAdapter();
            string nom = txtNom.Text;
            string prenom = txtPrenom.Text;
            string tel = txtTel.Text;
            string email = txtEmail.Text;
            string password = txtMDP.Text;
            string passwordConfirm = txtConfirmerMDP.Text;
            if(password != passwordConfirm)
            {
                MessageBox.Show("Les deux mots de passes sont différents !", "Mot de passes différents", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else if (nom == "" || prenom == "" || tel == "" || email == "" || password == "" ) {
                MessageBox.Show("Veuillez remplir tous les champs !", "Champ vide", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else if (utils.SelectByEmail(email) > 0)
            {
                MessageBox.Show("Cette adresse email est déjà utilisée", "Email déjà utilisé", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                utils.InsertUtil(nom, prenom, tel, email, password);
                MessageBox.Show("Utilisateur crée et ajouté à la base de donnée !", "Utilisateur ajouté", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }
    }
}
