﻿namespace GestionBoxeMDI
{
    partial class frmAjoutAbo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAjoutAbo = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblPrix = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtPrix = new System.Windows.Forms.TextBox();
            this.btnAjout = new System.Windows.Forms.Button();
            this.lblDuree = new System.Windows.Forms.Label();
            this.cboDuree = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblAjoutAbo
            // 
            this.lblAjoutAbo.AutoSize = true;
            this.lblAjoutAbo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAjoutAbo.Location = new System.Drawing.Point(63, 9);
            this.lblAjoutAbo.Name = "lblAjoutAbo";
            this.lblAjoutAbo.Size = new System.Drawing.Size(154, 17);
            this.lblAjoutAbo.TabIndex = 0;
            this.lblAjoutAbo.Text = "Ajout d\'un abonnement";
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Location = new System.Drawing.Point(55, 38);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(29, 13);
            this.lblNom.TabIndex = 1;
            this.lblNom.Text = "&Nom";
            // 
            // lblPrix
            // 
            this.lblPrix.AutoSize = true;
            this.lblPrix.Location = new System.Drawing.Point(60, 65);
            this.lblPrix.Name = "lblPrix";
            this.lblPrix.Size = new System.Drawing.Size(24, 13);
            this.lblPrix.TabIndex = 3;
            this.lblPrix.Text = "&Prix";
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(90, 34);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(100, 20);
            this.txtNom.TabIndex = 2;
            // 
            // txtPrix
            // 
            this.txtPrix.Location = new System.Drawing.Point(90, 61);
            this.txtPrix.Name = "txtPrix";
            this.txtPrix.Size = new System.Drawing.Size(100, 20);
            this.txtPrix.TabIndex = 4;
            // 
            // btnAjout
            // 
            this.btnAjout.Location = new System.Drawing.Point(90, 116);
            this.btnAjout.Name = "btnAjout";
            this.btnAjout.Size = new System.Drawing.Size(100, 23);
            this.btnAjout.TabIndex = 7;
            this.btnAjout.Text = "&Ajouter";
            this.btnAjout.UseVisualStyleBackColor = true;
            this.btnAjout.Click += new System.EventHandler(this.btnAjout_Click);
            // 
            // lblDuree
            // 
            this.lblDuree.AutoSize = true;
            this.lblDuree.Location = new System.Drawing.Point(48, 92);
            this.lblDuree.Name = "lblDuree";
            this.lblDuree.Size = new System.Drawing.Size(36, 13);
            this.lblDuree.TabIndex = 5;
            this.lblDuree.Text = "&Durée";
            // 
            // cboDuree
            // 
            this.cboDuree.FormattingEnabled = true;
            this.cboDuree.Items.AddRange(new object[] {
            "6 mois",
            "1 an"});
            this.cboDuree.Location = new System.Drawing.Point(90, 88);
            this.cboDuree.Name = "cboDuree";
            this.cboDuree.Size = new System.Drawing.Size(100, 21);
            this.cboDuree.TabIndex = 6;
            // 
            // frmAjoutAbo
            // 
            this.AcceptButton = this.btnAjout;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 152);
            this.Controls.Add(this.lblAjoutAbo);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.lblPrix);
            this.Controls.Add(this.lblDuree);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.txtPrix);
            this.Controls.Add(this.cboDuree);
            this.Controls.Add(this.btnAjout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmAjoutAbo";
            this.Text = "Ajout d\'un abonnement";
            this.Load += new System.EventHandler(this.frmAjoutAbo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAjoutAbo;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblPrix;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtPrix;
        private System.Windows.Forms.Button btnAjout;
        private System.Windows.Forms.Label lblDuree;
        private System.Windows.Forms.ComboBox cboDuree;
    }
}