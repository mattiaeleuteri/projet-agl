﻿namespace GestionBoxeMDI
{
    partial class frmAjoutCours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAjoutCours = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblDebut = new System.Windows.Forms.Label();
            this.lblSalle = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.btnAjout = new System.Windows.Forms.Button();
            this.lblFin = new System.Windows.Forms.Label();
            this.cboSalle = new System.Windows.Forms.ComboBox();
            this.cboDebut = new System.Windows.Forms.ComboBox();
            this.cboFin = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblAjoutCours
            // 
            this.lblAjoutCours.AutoSize = true;
            this.lblAjoutCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAjoutCours.Location = new System.Drawing.Point(92, 9);
            this.lblAjoutCours.Name = "lblAjoutCours";
            this.lblAjoutCours.Size = new System.Drawing.Size(110, 17);
            this.lblAjoutCours.TabIndex = 0;
            this.lblAjoutCours.Text = "Ajout d\'un cours";
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Location = new System.Drawing.Point(37, 38);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(29, 13);
            this.lblNom.TabIndex = 1;
            this.lblNom.Text = "&Nom";
            // 
            // lblDebut
            // 
            this.lblDebut.AutoSize = true;
            this.lblDebut.Location = new System.Drawing.Point(30, 65);
            this.lblDebut.Name = "lblDebut";
            this.lblDebut.Size = new System.Drawing.Size(36, 13);
            this.lblDebut.TabIndex = 3;
            this.lblDebut.Text = "&Début";
            // 
            // lblSalle
            // 
            this.lblSalle.AutoSize = true;
            this.lblSalle.Location = new System.Drawing.Point(36, 93);
            this.lblSalle.Name = "lblSalle";
            this.lblSalle.Size = new System.Drawing.Size(30, 13);
            this.lblSalle.TabIndex = 7;
            this.lblSalle.Text = "&Salle";
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(72, 34);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(150, 20);
            this.txtNom.TabIndex = 2;
            // 
            // btnAjout
            // 
            this.btnAjout.Location = new System.Drawing.Point(97, 117);
            this.btnAjout.Name = "btnAjout";
            this.btnAjout.Size = new System.Drawing.Size(100, 23);
            this.btnAjout.TabIndex = 9;
            this.btnAjout.Text = "&Ajouter";
            this.btnAjout.UseVisualStyleBackColor = true;
            this.btnAjout.Click += new System.EventHandler(this.btnAjout_Click);
            // 
            // lblFin
            // 
            this.lblFin.AutoSize = true;
            this.lblFin.Location = new System.Drawing.Point(147, 65);
            this.lblFin.Name = "lblFin";
            this.lblFin.Size = new System.Drawing.Size(21, 13);
            this.lblFin.TabIndex = 5;
            this.lblFin.Text = "&Fin";
            // 
            // cboSalle
            // 
            this.cboSalle.FormattingEnabled = true;
            this.cboSalle.Location = new System.Drawing.Point(72, 89);
            this.cboSalle.Name = "cboSalle";
            this.cboSalle.Size = new System.Drawing.Size(150, 21);
            this.cboSalle.TabIndex = 8;
            // 
            // cboDebut
            // 
            this.cboDebut.FormattingEnabled = true;
            this.cboDebut.Location = new System.Drawing.Point(72, 61);
            this.cboDebut.Name = "cboDebut";
            this.cboDebut.Size = new System.Drawing.Size(48, 21);
            this.cboDebut.TabIndex = 4;
            // 
            // cboFin
            // 
            this.cboFin.FormattingEnabled = true;
            this.cboFin.Location = new System.Drawing.Point(174, 61);
            this.cboFin.Name = "cboFin";
            this.cboFin.Size = new System.Drawing.Size(48, 21);
            this.cboFin.TabIndex = 6;
            // 
            // frmAjoutCours
            // 
            this.AcceptButton = this.btnAjout;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 155);
            this.Controls.Add(this.lblAjoutCours);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.lblDebut);
            this.Controls.Add(this.cboDebut);
            this.Controls.Add(this.lblFin);
            this.Controls.Add(this.cboFin);
            this.Controls.Add(this.lblSalle);
            this.Controls.Add(this.cboSalle);
            this.Controls.Add(this.btnAjout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmAjoutCours";
            this.Text = "Ajout d\'un cours";
            this.Load += new System.EventHandler(this.frmAjoutCours_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAjoutCours;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblDebut;
        private System.Windows.Forms.Label lblSalle;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Button btnAjout;
        private System.Windows.Forms.Label lblFin;
        private System.Windows.Forms.ComboBox cboSalle;
        private System.Windows.Forms.ComboBox cboDebut;
        private System.Windows.Forms.ComboBox cboFin;
    }
}