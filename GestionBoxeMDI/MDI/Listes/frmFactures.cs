﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmFactures : Form
    {
        public frmFactures()
        {
            InitializeComponent();
        }

        private void frmFactures_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'tables.FACTURE'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.fACTURETableAdapter.FillByOpenAll(this.tables.FACTURE);
            cboEtatFacture.SelectedIndex = 1;
        }

        private void cboEtatFacture_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboEtatFacture.SelectedIndex == 1)
            {
                this.fACTURETableAdapter.FillByOpenAll(this.tables.FACTURE);
            }
            else if (cboEtatFacture.SelectedIndex == 2)
            {
                this.fACTURETableAdapter.FillByClosedAll(this.tables.FACTURE);
            }
            else
            {
                this.fACTURETableAdapter.FillByAll(this.tables.FACTURE);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (cboEtatFacture.SelectedIndex == 1)
            {
                if (txtSearch.Text.Length == 0)
                    this.fACTURETableAdapter.FillByOpenAll(this.tables.FACTURE);
                else
                {
                    int value;
                    if (int.TryParse(txtSearch.Text, out value))
                        this.fACTURETableAdapter.FillBySearchOpen(this.tables.FACTURE, value);
                    else
                        this.fACTURETableAdapter.FillByOpenAll(this.tables.FACTURE);
                }
            }
            else if (cboEtatFacture.SelectedIndex == 2)
            {
                if (txtSearch.Text.Length == 0)
                    this.fACTURETableAdapter.FillByClosedAll(this.tables.FACTURE);
                else
                {
                    int value;
                    if (int.TryParse(txtSearch.Text, out value))
                        this.fACTURETableAdapter.FillBySearchClosed(this.tables.FACTURE, value);
                    else
                        this.fACTURETableAdapter.FillByClosedAll(this.tables.FACTURE);
                }
            }
            else
            {
                if (txtSearch.Text.Length == 0)
                    this.fACTURETableAdapter.FillByAll(this.tables.FACTURE);
                else
                {
                    int value;
                    if (int.TryParse(txtSearch.Text, out value))
                        this.fACTURETableAdapter.FillBySearchAll(this.tables.FACTURE, value);
                    else
                        this.fACTURETableAdapter.FillByAll(this.tables.FACTURE);
                }
            }
        }
    }
}
