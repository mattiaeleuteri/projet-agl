﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmCours : Form
    {
        public frmCours()
        {
            InitializeComponent();
        }

        private void btnAjout_Click(object sender, EventArgs e)
        {
            frmAjoutCours F = new frmAjoutCours();
            F.MdiParent = this.MdiParent;
            F.Show();
            F.Focus();
        }

        private void frmCours_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'tables1.VW_COURS'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.vW_COURSTableAdapter.FillBy(this.tables1.VW_COURS);

        }

        private void frmCours_Shown(object sender, EventArgs e)
        {
            if (Session.droitId == 1)
            {
                btnAjout.Visible = false;
            }
        }
    }
}
