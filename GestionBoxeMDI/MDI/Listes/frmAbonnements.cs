﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmAbonnements : Form
    {
        public frmAbonnements()
        {
            InitializeComponent();
        }

        private void frmAbonnements_Load(object sender, EventArgs e)
        {
            if (Session.droitId != 3)
            {
                this.aBONNEMENTTableAdapter.FillByUser(this.tables.ABONNEMENT, Session.aboId);
            }
            else
            {
                this.aBONNEMENTTableAdapter.FillByAll(this.tables.ABONNEMENT);
            }

        }

        private void btnAjout_Click(object sender, EventArgs e)
        {
            frmAjoutAbo f = new frmAjoutAbo();
            f.ShowDialog();
            f.Focus();
            this.aBONNEMENTTableAdapter.FillByAll(this.tables.ABONNEMENT);
        }

        private void frmAbonnements_Shown(object sender, EventArgs e)
        {
             if(Session.droitId != 3)
            {
                btnAjout.Visible = false;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (Session.droitId != 3)
            {
                this.aBONNEMENTTableAdapter.FillBySearchAndUser(this.tables.ABONNEMENT, txtSearch.Text, Session.aboId);
            }
            else
            {
                this.aBONNEMENTTableAdapter.FillBySearch(this.tables.ABONNEMENT, txtSearch.Text);
            }
        }
    }
}
