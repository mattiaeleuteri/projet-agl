﻿using System;
using System.Data;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmMembres : Form
    {
        public frmMembres()
        {
            InitializeComponent();
        }

        private void btnAjout_Click(object sender, EventArgs e)
        {
            frmAjoutMembre f = new frmAjoutMembre();
            f.MdiParent = this.MdiParent;
            f.Show();
            f.Focus();
        }

        private void frmMembres_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'tables.SE_DÉROULE'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.sE_DÉROULETableAdapter.Fill(this.tables.SE_DÉROULE);
            this.uTILISATEURTableAdapter.FillByUtilisateursActifs(this.tables.UTILISATEUR);
        }

        private void chkMembresActifs_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMembresActifs.Checked)
            {
                if (txtSearch.Text != "")
                {
                    this.uTILISATEURTableAdapter.FillBySearchActifs(this.tables.UTILISATEUR, txtSearch.Text);
                }
                else
                {
                    this.uTILISATEURTableAdapter.FillByUtilisateursActifs(this.tables.UTILISATEUR);
                }
            }
            else
            {
                if (txtSearch.Text != "")
                {
                    this.uTILISATEURTableAdapter.FillBySearch(this.tables.UTILISATEUR, txtSearch.Text);
                } else
                {
                    this.uTILISATEURTableAdapter.Fill(this.tables.UTILISATEUR);
                }
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (chkMembresActifs.Checked)
            {
                this.uTILISATEURTableAdapter.FillBySearchActifs(this.tables.UTILISATEUR, txtSearch.Text);
            }
            else
            {
                this.uTILISATEURTableAdapter.FillBySearch(this.tables.UTILISATEUR, txtSearch.Text);
            }
        }
    }
}
