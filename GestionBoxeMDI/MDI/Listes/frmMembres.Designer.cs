﻿namespace GestionBoxeMDI
{
    partial class frmMembres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkMembresActifs = new System.Windows.Forms.CheckBox();
            this.dgvMembres = new System.Windows.Forms.DataGridView();
            this.uTILIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dROITIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uTILEMAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uTILNOMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uTILPRENOMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uTILTELDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aBOIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uTILISATEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tables = new GestionBoxeMDI.tables();
            this.lblMembres = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnAjout = new System.Windows.Forms.Button();
            this.uTILISATEURTableAdapter = new GestionBoxeMDI.tablesTableAdapters.UTILISATEURTableAdapter();
            this.sEDÉROULEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sE_DÉROULETableAdapter = new GestionBoxeMDI.tablesTableAdapters.SE_DÉROULETableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMembres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTILISATEURBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sEDÉROULEBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // chkMembresActifs
            // 
            this.chkMembresActifs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkMembresActifs.AutoSize = true;
            this.chkMembresActifs.Checked = true;
            this.chkMembresActifs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMembresActifs.Location = new System.Drawing.Point(602, 13);
            this.chkMembresActifs.Name = "chkMembresActifs";
            this.chkMembresActifs.Size = new System.Drawing.Size(52, 17);
            this.chkMembresActifs.TabIndex = 3;
            this.chkMembresActifs.Text = "&Actifs";
            this.chkMembresActifs.UseVisualStyleBackColor = true;
            this.chkMembresActifs.CheckedChanged += new System.EventHandler(this.chkMembresActifs_CheckedChanged);
            // 
            // dgvMembres
            // 
            this.dgvMembres.AllowUserToAddRows = false;
            this.dgvMembres.AllowUserToDeleteRows = false;
            this.dgvMembres.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMembres.AutoGenerateColumns = false;
            this.dgvMembres.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMembres.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.uTILIDDataGridViewTextBoxColumn,
            this.dROITIDDataGridViewTextBoxColumn,
            this.uTILEMAILDataGridViewTextBoxColumn,
            this.uTILNOMDataGridViewTextBoxColumn,
            this.uTILPRENOMDataGridViewTextBoxColumn,
            this.uTILTELDataGridViewTextBoxColumn,
            this.aBOIDDataGridViewTextBoxColumn});
            this.dgvMembres.DataSource = this.uTILISATEURBindingSource;
            this.dgvMembres.Location = new System.Drawing.Point(15, 37);
            this.dgvMembres.Name = "dgvMembres";
            this.dgvMembres.ReadOnly = true;
            this.dgvMembres.RowHeadersVisible = false;
            this.dgvMembres.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMembres.Size = new System.Drawing.Size(642, 391);
            this.dgvMembres.TabIndex = 4;
            // 
            // uTILIDDataGridViewTextBoxColumn
            // 
            this.uTILIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.uTILIDDataGridViewTextBoxColumn.DataPropertyName = "UTIL_ID";
            this.uTILIDDataGridViewTextBoxColumn.HeaderText = "Numéro";
            this.uTILIDDataGridViewTextBoxColumn.Name = "uTILIDDataGridViewTextBoxColumn";
            this.uTILIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.uTILIDDataGridViewTextBoxColumn.Width = 69;
            // 
            // dROITIDDataGridViewTextBoxColumn
            // 
            this.dROITIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dROITIDDataGridViewTextBoxColumn.DataPropertyName = "DROIT_ID";
            this.dROITIDDataGridViewTextBoxColumn.HeaderText = "Droit";
            this.dROITIDDataGridViewTextBoxColumn.Name = "dROITIDDataGridViewTextBoxColumn";
            this.dROITIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.dROITIDDataGridViewTextBoxColumn.Width = 54;
            // 
            // uTILEMAILDataGridViewTextBoxColumn
            // 
            this.uTILEMAILDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uTILEMAILDataGridViewTextBoxColumn.DataPropertyName = "UTIL_EMAIL";
            this.uTILEMAILDataGridViewTextBoxColumn.HeaderText = "Email";
            this.uTILEMAILDataGridViewTextBoxColumn.Name = "uTILEMAILDataGridViewTextBoxColumn";
            this.uTILEMAILDataGridViewTextBoxColumn.ReadOnly = true;
            this.uTILEMAILDataGridViewTextBoxColumn.Width = 57;
            // 
            // uTILNOMDataGridViewTextBoxColumn
            // 
            this.uTILNOMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uTILNOMDataGridViewTextBoxColumn.DataPropertyName = "UTIL_NOM";
            this.uTILNOMDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.uTILNOMDataGridViewTextBoxColumn.Name = "uTILNOMDataGridViewTextBoxColumn";
            this.uTILNOMDataGridViewTextBoxColumn.ReadOnly = true;
            this.uTILNOMDataGridViewTextBoxColumn.Width = 54;
            // 
            // uTILPRENOMDataGridViewTextBoxColumn
            // 
            this.uTILPRENOMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uTILPRENOMDataGridViewTextBoxColumn.DataPropertyName = "UTIL_PRENOM";
            this.uTILPRENOMDataGridViewTextBoxColumn.HeaderText = "Prénom";
            this.uTILPRENOMDataGridViewTextBoxColumn.Name = "uTILPRENOMDataGridViewTextBoxColumn";
            this.uTILPRENOMDataGridViewTextBoxColumn.ReadOnly = true;
            this.uTILPRENOMDataGridViewTextBoxColumn.Width = 68;
            // 
            // uTILTELDataGridViewTextBoxColumn
            // 
            this.uTILTELDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.uTILTELDataGridViewTextBoxColumn.DataPropertyName = "UTIL_TEL";
            this.uTILTELDataGridViewTextBoxColumn.HeaderText = "Téléphone";
            this.uTILTELDataGridViewTextBoxColumn.Name = "uTILTELDataGridViewTextBoxColumn";
            this.uTILTELDataGridViewTextBoxColumn.ReadOnly = true;
            this.uTILTELDataGridViewTextBoxColumn.Width = 83;
            // 
            // aBOIDDataGridViewTextBoxColumn
            // 
            this.aBOIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.aBOIDDataGridViewTextBoxColumn.DataPropertyName = "ABO_ID";
            this.aBOIDDataGridViewTextBoxColumn.HeaderText = "Abonnement";
            this.aBOIDDataGridViewTextBoxColumn.Name = "aBOIDDataGridViewTextBoxColumn";
            this.aBOIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uTILISATEURBindingSource
            // 
            this.uTILISATEURBindingSource.DataMember = "UTILISATEUR";
            this.uTILISATEURBindingSource.DataSource = this.tables;
            // 
            // tables
            // 
            this.tables.DataSetName = "tables";
            this.tables.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblMembres
            // 
            this.lblMembres.AutoSize = true;
            this.lblMembres.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMembres.Location = new System.Drawing.Point(12, 13);
            this.lblMembres.Name = "lblMembres";
            this.lblMembres.Size = new System.Drawing.Size(127, 17);
            this.lblMembres.TabIndex = 0;
            this.lblMembres.Text = "Liste des membres";
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(438, 11);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(148, 20);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnAjout
            // 
            this.btnAjout.Location = new System.Drawing.Point(145, 10);
            this.btnAjout.Name = "btnAjout";
            this.btnAjout.Size = new System.Drawing.Size(69, 23);
            this.btnAjout.TabIndex = 1;
            this.btnAjout.Text = "Ajouter";
            this.btnAjout.UseVisualStyleBackColor = true;
            this.btnAjout.Click += new System.EventHandler(this.btnAjout_Click);
            // 
            // uTILISATEURTableAdapter
            // 
            this.uTILISATEURTableAdapter.ClearBeforeFill = true;
            // 
            // sEDÉROULEBindingSource
            // 
            this.sEDÉROULEBindingSource.DataMember = "SE_DÉROULE";
            this.sEDÉROULEBindingSource.DataSource = this.tables;
            // 
            // sE_DÉROULETableAdapter
            // 
            this.sE_DÉROULETableAdapter.ClearBeforeFill = true;
            // 
            // frmMembres
            // 
            this.AcceptButton = this.btnAjout;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 442);
            this.Controls.Add(this.btnAjout);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.chkMembresActifs);
            this.Controls.Add(this.dgvMembres);
            this.Controls.Add(this.lblMembres);
            this.MinimumSize = new System.Drawing.Size(676, 473);
            this.Name = "frmMembres";
            this.Text = "Liste des membres";
            this.Load += new System.EventHandler(this.frmMembres_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMembres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTILISATEURBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sEDÉROULEBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkMembresActifs;
        private System.Windows.Forms.DataGridView dgvMembres;
        private System.Windows.Forms.Label lblMembres;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnAjout;
        private tables tables;
        private System.Windows.Forms.BindingSource uTILISATEURBindingSource;
        private tablesTableAdapters.UTILISATEURTableAdapter uTILISATEURTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn uTILIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dROITIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uTILEMAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uTILNOMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uTILPRENOMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uTILTELDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aBOIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource sEDÉROULEBindingSource;
        private tablesTableAdapters.SE_DÉROULETableAdapter sE_DÉROULETableAdapter;
    }
}