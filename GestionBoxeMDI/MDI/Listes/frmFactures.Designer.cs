﻿namespace GestionBoxeMDI
{
    partial class frmFactures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvFactures = new System.Windows.Forms.DataGridView();
            this.fACTUREBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tables = new GestionBoxeMDI.tables();
            this.lblFactures = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.cboEtatFacture = new System.Windows.Forms.ComboBox();
            this.fACTURETableAdapter = new GestionBoxeMDI.tablesTableAdapters.FACTURETableAdapter();
            this.fACTUREIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fACTUREMONTANTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fACTUREECHEANCEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fACTURESTATUTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fACTUREBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFactures
            // 
            this.dgvFactures.AllowUserToAddRows = false;
            this.dgvFactures.AllowUserToDeleteRows = false;
            this.dgvFactures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFactures.AutoGenerateColumns = false;
            this.dgvFactures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFactures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fACTUREIDDataGridViewTextBoxColumn,
            this.fACTUREMONTANTDataGridViewTextBoxColumn,
            this.fACTUREECHEANCEDataGridViewTextBoxColumn,
            this.fACTURESTATUTDataGridViewTextBoxColumn});
            this.dgvFactures.DataSource = this.fACTUREBindingSource;
            this.dgvFactures.Location = new System.Drawing.Point(12, 39);
            this.dgvFactures.Name = "dgvFactures";
            this.dgvFactures.ReadOnly = true;
            this.dgvFactures.RowHeadersVisible = false;
            this.dgvFactures.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFactures.Size = new System.Drawing.Size(635, 383);
            this.dgvFactures.TabIndex = 3;
            // 
            // fACTUREBindingSource
            // 
            this.fACTUREBindingSource.DataMember = "FACTURE";
            this.fACTUREBindingSource.DataSource = this.tables;
            // 
            // tables
            // 
            this.tables.DataSetName = "tables";
            this.tables.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblFactures
            // 
            this.lblFactures.AutoSize = true;
            this.lblFactures.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactures.Location = new System.Drawing.Point(12, 13);
            this.lblFactures.Name = "lblFactures";
            this.lblFactures.Size = new System.Drawing.Size(87, 17);
            this.lblFactures.TabIndex = 0;
            this.lblFactures.Text = "Vos factures";
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(343, 11);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(148, 20);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // cboEtatFacture
            // 
            this.cboEtatFacture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboEtatFacture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEtatFacture.FormattingEnabled = true;
            this.cboEtatFacture.Items.AddRange(new object[] {
            "Toutes les factures",
            "Ouvertes",
            "Fermées"});
            this.cboEtatFacture.Location = new System.Drawing.Point(507, 11);
            this.cboEtatFacture.Name = "cboEtatFacture";
            this.cboEtatFacture.Size = new System.Drawing.Size(140, 21);
            this.cboEtatFacture.TabIndex = 2;
            this.cboEtatFacture.SelectedIndexChanged += new System.EventHandler(this.cboEtatFacture_SelectedIndexChanged);
            // 
            // fACTURETableAdapter
            // 
            this.fACTURETableAdapter.ClearBeforeFill = true;
            // 
            // fACTUREIDDataGridViewTextBoxColumn
            // 
            this.fACTUREIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.fACTUREIDDataGridViewTextBoxColumn.DataPropertyName = "FACTURE_ID";
            this.fACTUREIDDataGridViewTextBoxColumn.HeaderText = "Numéro";
            this.fACTUREIDDataGridViewTextBoxColumn.Name = "fACTUREIDDataGridViewTextBoxColumn";
            this.fACTUREIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.fACTUREIDDataGridViewTextBoxColumn.Width = 69;
            // 
            // fACTUREMONTANTDataGridViewTextBoxColumn
            // 
            this.fACTUREMONTANTDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fACTUREMONTANTDataGridViewTextBoxColumn.DataPropertyName = "FACTURE_MONTANT";
            this.fACTUREMONTANTDataGridViewTextBoxColumn.HeaderText = "Montant";
            this.fACTUREMONTANTDataGridViewTextBoxColumn.Name = "fACTUREMONTANTDataGridViewTextBoxColumn";
            this.fACTUREMONTANTDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fACTUREECHEANCEDataGridViewTextBoxColumn
            // 
            this.fACTUREECHEANCEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fACTUREECHEANCEDataGridViewTextBoxColumn.DataPropertyName = "FACTURE_ECHEANCE";
            this.fACTUREECHEANCEDataGridViewTextBoxColumn.HeaderText = "Echéance";
            this.fACTUREECHEANCEDataGridViewTextBoxColumn.Name = "fACTUREECHEANCEDataGridViewTextBoxColumn";
            this.fACTUREECHEANCEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fACTURESTATUTDataGridViewTextBoxColumn
            // 
            this.fACTURESTATUTDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fACTURESTATUTDataGridViewTextBoxColumn.DataPropertyName = "FACTURE_STATUT";
            this.fACTURESTATUTDataGridViewTextBoxColumn.HeaderText = "Statut";
            this.fACTURESTATUTDataGridViewTextBoxColumn.Name = "fACTURESTATUTDataGridViewTextBoxColumn";
            this.fACTURESTATUTDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // frmFactures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 434);
            this.Controls.Add(this.cboEtatFacture);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.dgvFactures);
            this.Controls.Add(this.lblFactures);
            this.MinimumSize = new System.Drawing.Size(676, 473);
            this.Name = "frmFactures";
            this.Text = "Vos factures";
            this.Load += new System.EventHandler(this.frmFactures_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fACTUREBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFactures;
        private System.Windows.Forms.Label lblFactures;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.ComboBox cboEtatFacture;
        private tables tables;
        private System.Windows.Forms.BindingSource fACTUREBindingSource;
        private tablesTableAdapters.FACTURETableAdapter fACTURETableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn fACTUREIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fACTUREMONTANTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fACTUREECHEANCEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fACTURESTATUTDataGridViewTextBoxColumn;
    }
}