﻿namespace GestionBoxeMDI
{
    partial class frmCours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblListeCours = new System.Windows.Forms.Label();
            this.dgvCours = new System.Windows.Forms.DataGridView();
            this.vWCOURSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tables1 = new GestionBoxeMDI.tables();
            this.cOURSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tables = new GestionBoxeMDI.tables();
            this.chkInscrit = new System.Windows.Forms.CheckBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnAjout = new System.Windows.Forms.Button();
            this.cOURSTableAdapter = new GestionBoxeMDI.tablesTableAdapters.COURSTableAdapter();
            this.vW_COURSTableAdapter = new GestionBoxeMDI.tablesTableAdapters.VW_COURSTableAdapter();
            this.fINDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dEBUTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jOURDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sALLEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nUMERODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vWCOURSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOURSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables)).BeginInit();
            this.SuspendLayout();
            // 
            // lblListeCours
            // 
            this.lblListeCours.AutoSize = true;
            this.lblListeCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblListeCours.Location = new System.Drawing.Point(12, 13);
            this.lblListeCours.Name = "lblListeCours";
            this.lblListeCours.Size = new System.Drawing.Size(104, 17);
            this.lblListeCours.TabIndex = 0;
            this.lblListeCours.Text = "Liste des cours";
            // 
            // dgvCours
            // 
            this.dgvCours.AllowUserToAddRows = false;
            this.dgvCours.AllowUserToDeleteRows = false;
            this.dgvCours.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCours.AutoGenerateColumns = false;
            this.dgvCours.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCours.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nUMERODataGridViewTextBoxColumn,
            this.nOMDataGridViewTextBoxColumn,
            this.sALLEDataGridViewTextBoxColumn,
            this.jOURDataGridViewTextBoxColumn,
            this.dEBUTDataGridViewTextBoxColumn,
            this.fINDataGridViewTextBoxColumn});
            this.dgvCours.DataSource = this.vWCOURSBindingSource;
            this.dgvCours.Location = new System.Drawing.Point(12, 39);
            this.dgvCours.Name = "dgvCours";
            this.dgvCours.ReadOnly = true;
            this.dgvCours.RowHeadersVisible = false;
            this.dgvCours.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCours.Size = new System.Drawing.Size(635, 383);
            this.dgvCours.TabIndex = 4;
            // 
            // vWCOURSBindingSource
            // 
            this.vWCOURSBindingSource.DataMember = "VW_COURS";
            this.vWCOURSBindingSource.DataSource = this.tables1;
            // 
            // tables1
            // 
            this.tables1.DataSetName = "tables";
            this.tables1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cOURSBindingSource
            // 
            this.cOURSBindingSource.DataMember = "COURS";
            this.cOURSBindingSource.DataSource = this.tables;
            // 
            // tables
            // 
            this.tables.DataSetName = "tables";
            this.tables.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // chkInscrit
            // 
            this.chkInscrit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkInscrit.AutoSize = true;
            this.chkInscrit.Location = new System.Drawing.Point(593, 13);
            this.chkInscrit.Name = "chkInscrit";
            this.chkInscrit.Size = new System.Drawing.Size(54, 17);
            this.chkInscrit.TabIndex = 3;
            this.chkInscrit.Text = "&Inscrit";
            this.chkInscrit.UseVisualStyleBackColor = true;
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(429, 11);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(148, 20);
            this.txtSearch.TabIndex = 2;
            // 
            // btnAjout
            // 
            this.btnAjout.Location = new System.Drawing.Point(122, 10);
            this.btnAjout.Name = "btnAjout";
            this.btnAjout.Size = new System.Drawing.Size(69, 23);
            this.btnAjout.TabIndex = 1;
            this.btnAjout.Text = "Ajouter";
            this.btnAjout.UseVisualStyleBackColor = true;
            this.btnAjout.Click += new System.EventHandler(this.btnAjout_Click);
            // 
            // cOURSTableAdapter
            // 
            this.cOURSTableAdapter.ClearBeforeFill = true;
            // 
            // vW_COURSTableAdapter
            // 
            this.vW_COURSTableAdapter.ClearBeforeFill = true;
            // 
            // fINDataGridViewTextBoxColumn
            // 
            this.fINDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fINDataGridViewTextBoxColumn.DataPropertyName = "FIN";
            this.fINDataGridViewTextBoxColumn.HeaderText = "Fin";
            this.fINDataGridViewTextBoxColumn.Name = "fINDataGridViewTextBoxColumn";
            this.fINDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dEBUTDataGridViewTextBoxColumn
            // 
            this.dEBUTDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dEBUTDataGridViewTextBoxColumn.DataPropertyName = "DEBUT";
            this.dEBUTDataGridViewTextBoxColumn.HeaderText = "Début";
            this.dEBUTDataGridViewTextBoxColumn.Name = "dEBUTDataGridViewTextBoxColumn";
            this.dEBUTDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jOURDataGridViewTextBoxColumn
            // 
            this.jOURDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.jOURDataGridViewTextBoxColumn.DataPropertyName = "JOUR";
            this.jOURDataGridViewTextBoxColumn.HeaderText = "Jour";
            this.jOURDataGridViewTextBoxColumn.Name = "jOURDataGridViewTextBoxColumn";
            this.jOURDataGridViewTextBoxColumn.ReadOnly = true;
            this.jOURDataGridViewTextBoxColumn.Width = 52;
            // 
            // sALLEDataGridViewTextBoxColumn
            // 
            this.sALLEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.sALLEDataGridViewTextBoxColumn.DataPropertyName = "SALLE";
            this.sALLEDataGridViewTextBoxColumn.HeaderText = "Salle";
            this.sALLEDataGridViewTextBoxColumn.Name = "sALLEDataGridViewTextBoxColumn";
            this.sALLEDataGridViewTextBoxColumn.ReadOnly = true;
            this.sALLEDataGridViewTextBoxColumn.Width = 55;
            // 
            // nOMDataGridViewTextBoxColumn
            // 
            this.nOMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.nOMDataGridViewTextBoxColumn.DataPropertyName = "NOM";
            this.nOMDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nOMDataGridViewTextBoxColumn.Name = "nOMDataGridViewTextBoxColumn";
            this.nOMDataGridViewTextBoxColumn.ReadOnly = true;
            this.nOMDataGridViewTextBoxColumn.Width = 54;
            // 
            // nUMERODataGridViewTextBoxColumn
            // 
            this.nUMERODataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.nUMERODataGridViewTextBoxColumn.DataPropertyName = "NUMERO";
            this.nUMERODataGridViewTextBoxColumn.HeaderText = "Numéro";
            this.nUMERODataGridViewTextBoxColumn.Name = "nUMERODataGridViewTextBoxColumn";
            this.nUMERODataGridViewTextBoxColumn.ReadOnly = true;
            this.nUMERODataGridViewTextBoxColumn.Width = 69;
            // 
            // frmCours
            // 
            this.AcceptButton = this.btnAjout;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 434);
            this.Controls.Add(this.btnAjout);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.chkInscrit);
            this.Controls.Add(this.lblListeCours);
            this.Controls.Add(this.dgvCours);
            this.MinimumSize = new System.Drawing.Size(676, 473);
            this.Name = "frmCours";
            this.Text = "Liste des cours de la semaine";
            this.Load += new System.EventHandler(this.frmCours_Load);
            this.Shown += new System.EventHandler(this.frmCours_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vWCOURSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOURSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblListeCours;
        private System.Windows.Forms.DataGridView dgvCours;
        private System.Windows.Forms.CheckBox chkInscrit;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnAjout;
        private tables tables;
        private System.Windows.Forms.BindingSource cOURSBindingSource;
        private tablesTableAdapters.COURSTableAdapter cOURSTableAdapter;
        private tables tables1;
        private System.Windows.Forms.BindingSource vWCOURSBindingSource;
        private tablesTableAdapters.VW_COURSTableAdapter vW_COURSTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMERODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sALLEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jOURDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dEBUTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fINDataGridViewTextBoxColumn;
    }
}