﻿namespace GestionBoxeMDI
{
    partial class frmAbonnements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvAbo = new System.Windows.Forms.DataGridView();
            this.aBONNEMENTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tablesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tables = new GestionBoxeMDI.tables();
            this.lblAbonnements = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnAjout = new System.Windows.Forms.Button();
            this.aBONNEMENTTableAdapter = new GestionBoxeMDI.tablesTableAdapters.ABONNEMENTTableAdapter();
            this.aBOIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aBONOMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aBOPRIXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aBODUREEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aBONNEMENTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAbo
            // 
            this.dgvAbo.AllowUserToAddRows = false;
            this.dgvAbo.AllowUserToDeleteRows = false;
            this.dgvAbo.AllowUserToResizeColumns = false;
            this.dgvAbo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAbo.AutoGenerateColumns = false;
            this.dgvAbo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAbo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.aBOIDDataGridViewTextBoxColumn,
            this.aBONOMDataGridViewTextBoxColumn,
            this.aBOPRIXDataGridViewTextBoxColumn,
            this.aBODUREEDataGridViewTextBoxColumn});
            this.dgvAbo.DataSource = this.aBONNEMENTBindingSource;
            this.dgvAbo.Location = new System.Drawing.Point(12, 39);
            this.dgvAbo.Name = "dgvAbo";
            this.dgvAbo.ReadOnly = true;
            this.dgvAbo.RowHeadersVisible = false;
            this.dgvAbo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAbo.Size = new System.Drawing.Size(635, 383);
            this.dgvAbo.TabIndex = 3;
            // 
            // aBONNEMENTBindingSource
            // 
            this.aBONNEMENTBindingSource.DataMember = "ABONNEMENT";
            this.aBONNEMENTBindingSource.DataSource = this.tablesBindingSource;
            // 
            // tablesBindingSource
            // 
            this.tablesBindingSource.DataSource = this.tables;
            this.tablesBindingSource.Position = 0;
            // 
            // tables
            // 
            this.tables.DataSetName = "tables";
            this.tables.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblAbonnements
            // 
            this.lblAbonnements.AutoSize = true;
            this.lblAbonnements.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAbonnements.Location = new System.Drawing.Point(12, 13);
            this.lblAbonnements.Name = "lblAbonnements";
            this.lblAbonnements.Size = new System.Drawing.Size(122, 17);
            this.lblAbonnements.TabIndex = 0;
            this.lblAbonnements.Text = "Vos abonnements";
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(499, 11);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(148, 20);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnAjout
            // 
            this.btnAjout.Location = new System.Drawing.Point(140, 10);
            this.btnAjout.Name = "btnAjout";
            this.btnAjout.Size = new System.Drawing.Size(69, 23);
            this.btnAjout.TabIndex = 1;
            this.btnAjout.Text = "Ajouter";
            this.btnAjout.UseVisualStyleBackColor = true;
            this.btnAjout.Click += new System.EventHandler(this.btnAjout_Click);
            // 
            // aBONNEMENTTableAdapter
            // 
            this.aBONNEMENTTableAdapter.ClearBeforeFill = true;
            // 
            // aBOIDDataGridViewTextBoxColumn
            // 
            this.aBOIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.aBOIDDataGridViewTextBoxColumn.DataPropertyName = "ABO_ID";
            this.aBOIDDataGridViewTextBoxColumn.HeaderText = "Numéro";
            this.aBOIDDataGridViewTextBoxColumn.Name = "aBOIDDataGridViewTextBoxColumn";
            this.aBOIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.aBOIDDataGridViewTextBoxColumn.Width = 69;
            // 
            // aBONOMDataGridViewTextBoxColumn
            // 
            this.aBONOMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.aBONOMDataGridViewTextBoxColumn.DataPropertyName = "ABO_NOM";
            this.aBONOMDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.aBONOMDataGridViewTextBoxColumn.Name = "aBONOMDataGridViewTextBoxColumn";
            this.aBONOMDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // aBOPRIXDataGridViewTextBoxColumn
            // 
            this.aBOPRIXDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.aBOPRIXDataGridViewTextBoxColumn.DataPropertyName = "ABO_PRIX";
            this.aBOPRIXDataGridViewTextBoxColumn.HeaderText = "Prix";
            this.aBOPRIXDataGridViewTextBoxColumn.Name = "aBOPRIXDataGridViewTextBoxColumn";
            this.aBOPRIXDataGridViewTextBoxColumn.ReadOnly = true;
            this.aBOPRIXDataGridViewTextBoxColumn.Width = 49;
            // 
            // aBODUREEDataGridViewTextBoxColumn
            // 
            this.aBODUREEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.aBODUREEDataGridViewTextBoxColumn.DataPropertyName = "ABO_DUREE";
            this.aBODUREEDataGridViewTextBoxColumn.HeaderText = "Durée";
            this.aBODUREEDataGridViewTextBoxColumn.Name = "aBODUREEDataGridViewTextBoxColumn";
            this.aBODUREEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // frmAbonnements
            // 
            this.AcceptButton = this.btnAjout;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 434);
            this.Controls.Add(this.btnAjout);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.dgvAbo);
            this.Controls.Add(this.lblAbonnements);
            this.MinimumSize = new System.Drawing.Size(676, 473);
            this.Name = "frmAbonnements";
            this.Text = "Vos abonnements";
            this.Load += new System.EventHandler(this.frmAbonnements_Load);
            this.Shown += new System.EventHandler(this.frmAbonnements_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAbo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aBONNEMENTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tables)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAbo;
        private System.Windows.Forms.Label lblAbonnements;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnAjout;
        private System.Windows.Forms.BindingSource tablesBindingSource;
        private tables tables;
        private System.Windows.Forms.BindingSource aBONNEMENTBindingSource;
        private tablesTableAdapters.ABONNEMENTTableAdapter aBONNEMENTTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn aBOIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aBONOMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aBOPRIXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aBODUREEDataGridViewTextBoxColumn;
    }
}