﻿namespace GestionBoxeMDI
{
    partial class PrincipalMDI
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.mnsMDI = new System.Windows.Forms.MenuStrip();
            this.tsmMDIApplication = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmApplicationFermer = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMDICours = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCoursListeDesCours = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMDIFactures = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFacturesVosFactures = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMDIAbonnements = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAbonnementsVosAbonnements = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMDIMembres = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMembresListeDesMembres = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMDIAffichage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAffichageVertical = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAffichageHorizontal = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAffichageCascade = new System.Windows.Forms.ToolStripMenuItem();
            this.ssrUtilisateur = new System.Windows.Forms.StatusStrip();
            this.ssrLblNomUtilisateur = new System.Windows.Forms.ToolStripStatusLabel();
            this.mnsMDI.SuspendLayout();
            this.ssrUtilisateur.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnsMDI
            // 
            this.mnsMDI.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmMDIApplication,
            this.tsmMDICours,
            this.tsmMDIFactures,
            this.tsmMDIAbonnements,
            this.tsmMDIMembres,
            this.tsmMDIAffichage});
            this.mnsMDI.Location = new System.Drawing.Point(0, 0);
            this.mnsMDI.Name = "mnsMDI";
            this.mnsMDI.Size = new System.Drawing.Size(847, 24);
            this.mnsMDI.TabIndex = 0;
            this.mnsMDI.Text = "MenuStrip";
            // 
            // tsmMDIApplication
            // 
            this.tsmMDIApplication.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmApplicationFermer});
            this.tsmMDIApplication.Name = "tsmMDIApplication";
            this.tsmMDIApplication.Size = new System.Drawing.Size(80, 20);
            this.tsmMDIApplication.Text = "App&lication";
            this.tsmMDIApplication.Click += new System.EventHandler(this.tsmMDIApplication_Click);
            // 
            // tsmApplicationFermer
            // 
            this.tsmApplicationFermer.Name = "tsmApplicationFermer";
            this.tsmApplicationFermer.Size = new System.Drawing.Size(180, 22);
            this.tsmApplicationFermer.Text = "&Fermer";
            this.tsmApplicationFermer.Click += new System.EventHandler(this.fermerToolStripMenuItem_Click);
            // 
            // tsmMDICours
            // 
            this.tsmMDICours.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCoursListeDesCours});
            this.tsmMDICours.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.tsmMDICours.Name = "tsmMDICours";
            this.tsmMDICours.Size = new System.Drawing.Size(50, 20);
            this.tsmMDICours.Text = "&Cours";
            // 
            // tsmCoursListeDesCours
            // 
            this.tsmCoursListeDesCours.Name = "tsmCoursListeDesCours";
            this.tsmCoursListeDesCours.Size = new System.Drawing.Size(151, 22);
            this.tsmCoursListeDesCours.Text = "&Liste des cours";
            this.tsmCoursListeDesCours.Click += new System.EventHandler(this.listeDesCoursToolStripMenuItem_Click);
            // 
            // tsmMDIFactures
            // 
            this.tsmMDIFactures.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFacturesVosFactures});
            this.tsmMDIFactures.Name = "tsmMDIFactures";
            this.tsmMDIFactures.Size = new System.Drawing.Size(63, 20);
            this.tsmMDIFactures.Text = "&Factures";
            // 
            // tsmFacturesVosFactures
            // 
            this.tsmFacturesVosFactures.Name = "tsmFacturesVosFactures";
            this.tsmFacturesVosFactures.Size = new System.Drawing.Size(137, 22);
            this.tsmFacturesVosFactures.Text = "&Vos factures";
            this.tsmFacturesVosFactures.Click += new System.EventHandler(this.vosFacturesToolStripMenuItem_Click);
            // 
            // tsmMDIAbonnements
            // 
            this.tsmMDIAbonnements.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAbonnementsVosAbonnements});
            this.tsmMDIAbonnements.Name = "tsmMDIAbonnements";
            this.tsmMDIAbonnements.Size = new System.Drawing.Size(94, 20);
            this.tsmMDIAbonnements.Text = "&Abonnements";
            // 
            // tsmAbonnementsVosAbonnements
            // 
            this.tsmAbonnementsVosAbonnements.Name = "tsmAbonnementsVosAbonnements";
            this.tsmAbonnementsVosAbonnements.Size = new System.Drawing.Size(168, 22);
            this.tsmAbonnementsVosAbonnements.Text = "&Vos abonnements";
            this.tsmAbonnementsVosAbonnements.Click += new System.EventHandler(this.vosAbonnementsToolStripMenuItem_Click);
            // 
            // tsmMDIMembres
            // 
            this.tsmMDIMembres.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmMembresListeDesMembres});
            this.tsmMDIMembres.Name = "tsmMDIMembres";
            this.tsmMDIMembres.Size = new System.Drawing.Size(69, 20);
            this.tsmMDIMembres.Text = "&Membres";
            // 
            // tsmMembresListeDesMembres
            // 
            this.tsmMembresListeDesMembres.Name = "tsmMembresListeDesMembres";
            this.tsmMembresListeDesMembres.Size = new System.Drawing.Size(172, 22);
            this.tsmMembresListeDesMembres.Text = "&Liste des membres";
            this.tsmMembresListeDesMembres.Click += new System.EventHandler(this.listeDesMembresToolStripMenuItem_Click);
            // 
            // tsmMDIAffichage
            // 
            this.tsmMDIAffichage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAffichageVertical,
            this.tsmAffichageHorizontal,
            this.tsmAffichageCascade});
            this.tsmMDIAffichage.Name = "tsmMDIAffichage";
            this.tsmMDIAffichage.Size = new System.Drawing.Size(70, 20);
            this.tsmMDIAffichage.Text = "Affic&hage";
            // 
            // tsmAffichageVertical
            // 
            this.tsmAffichageVertical.Name = "tsmAffichageVertical";
            this.tsmAffichageVertical.Size = new System.Drawing.Size(129, 22);
            this.tsmAffichageVertical.Text = "&Vertical";
            this.tsmAffichageVertical.Click += new System.EventHandler(this.verticalToolStripMenuItem_Click);
            // 
            // tsmAffichageHorizontal
            // 
            this.tsmAffichageHorizontal.Name = "tsmAffichageHorizontal";
            this.tsmAffichageHorizontal.Size = new System.Drawing.Size(129, 22);
            this.tsmAffichageHorizontal.Text = "&Horizontal";
            this.tsmAffichageHorizontal.Click += new System.EventHandler(this.horizontalToolStripMenuItem_Click);
            // 
            // tsmAffichageCascade
            // 
            this.tsmAffichageCascade.Name = "tsmAffichageCascade";
            this.tsmAffichageCascade.Size = new System.Drawing.Size(129, 22);
            this.tsmAffichageCascade.Text = "&Cascade";
            this.tsmAffichageCascade.Click += new System.EventHandler(this.cascadeToolStripMenuItem_Click);
            // 
            // ssrUtilisateur
            // 
            this.ssrUtilisateur.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ssrLblNomUtilisateur});
            this.ssrUtilisateur.Location = new System.Drawing.Point(0, 536);
            this.ssrUtilisateur.Name = "ssrUtilisateur";
            this.ssrUtilisateur.Size = new System.Drawing.Size(847, 22);
            this.ssrUtilisateur.TabIndex = 1;
            this.ssrUtilisateur.Text = "StatusStrip";
            // 
            // ssrLblNomUtilisateur
            // 
            this.ssrLblNomUtilisateur.Name = "ssrLblNomUtilisateur";
            this.ssrLblNomUtilisateur.Size = new System.Drawing.Size(30, 17);
            this.ssrLblNomUtilisateur.Text = "User";
            this.ssrLblNomUtilisateur.Click += new System.EventHandler(this.ssrLblNomUtilisateur_Click);
            // 
            // PrincipalMDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 558);
            this.Controls.Add(this.ssrUtilisateur);
            this.Controls.Add(this.mnsMDI);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnsMDI;
            this.Name = "PrincipalMDI";
            this.Text = "GestionBoxe";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.PrincipalMDI_Shown);
            this.mnsMDI.ResumeLayout(false);
            this.mnsMDI.PerformLayout();
            this.ssrUtilisateur.ResumeLayout(false);
            this.ssrUtilisateur.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip mnsMDI;
        private System.Windows.Forms.StatusStrip ssrUtilisateur;
        private System.Windows.Forms.ToolStripStatusLabel ssrLblNomUtilisateur;
        private System.Windows.Forms.ToolStripMenuItem tsmMDIFactures;
        private System.Windows.Forms.ToolStripMenuItem tsmMDIAbonnements;
        private System.Windows.Forms.ToolStripMenuItem tsmMDIApplication;
        private System.Windows.Forms.ToolStripMenuItem tsmApplicationFermer;
        private System.Windows.Forms.ToolStripMenuItem tsmMDICours;
        private System.Windows.Forms.ToolStripMenuItem tsmCoursListeDesCours;
        private System.Windows.Forms.ToolStripMenuItem tsmFacturesVosFactures;
        private System.Windows.Forms.ToolStripMenuItem tsmAbonnementsVosAbonnements;
        private System.Windows.Forms.ToolStripMenuItem tsmMDIAffichage;
        private System.Windows.Forms.ToolStripMenuItem tsmAffichageVertical;
        private System.Windows.Forms.ToolStripMenuItem tsmAffichageHorizontal;
        private System.Windows.Forms.ToolStripMenuItem tsmAffichageCascade;
        private System.Windows.Forms.ToolStripMenuItem tsmMDIMembres;
        private System.Windows.Forms.ToolStripMenuItem tsmMembresListeDesMembres;
    }
}



