﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmInscription : Form
    {
        public frmInscription()
        {
            InitializeComponent();
        }

        private void btnInscription_Click(object sender, EventArgs e)
        {
            tables tables = new tables();
            tablesTableAdapters.UTILISATEURTableAdapter tablesUtilisateurTableAdapter = new tablesTableAdapters.UTILISATEURTableAdapter();
            tablesUtilisateurTableAdapter.Fill(tables.UTILISATEUR);

            DataRow[] foundRows;
            foundRows = tables.Tables["UTILISATEUR"].Select("util_email = '" + txtInscEmail.Text + "'");
            if (foundRows.ToArray().Length == 0)
            {
                OracleConnection conn = new OracleConnection("DATA SOURCE=localhost;PERSIST SECURITY INFO=True;USER ID=GestionBoxe_data;PASSWORD=GestionBoxe_data;");
                try
                {
                    if (txtInscNom.Text != "" && txtInscPrenom.Text != "" && txtInscEmail.Text != "" && txtInscMDP.Text != "" && txtInscConfirmerMDP.Text != "" && txtInscTel.Text != "")
                    {
                        conn.Open();
                        OracleCommand command = new OracleCommand("INSERT INTO UTILISATEUR VALUES(null, null, 1, '" + txtInscNom.Text + "', '" + txtInscPrenom.Text + "', '" + txtInscTel.Text + "', '" + txtInscEmail.Text + "', '" + txtInscMDP.Text + "')", conn);
                        if (txtInscMDP.Text != txtInscConfirmerMDP.Text)
                        {
                            MessageBox.Show("Les deux mots de pass sont différents.", "Mots de pass différents", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                        }
                        else
                        {
                            try
                            {
                                command.ExecuteNonQuery();
                                MessageBox.Show("Félicitations, vous êtes inscrit ! Vous pouvez désormais vous connecter !", "Inscription validée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Close();
                            }
                            catch
                            {
                                MessageBox.Show("ERROR DURING AN INSERT");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Inscription incomplète, veuillez remplir les données d'inscription.", "Inscription incomplète", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {
                MessageBox.Show("Cet email est déjà utilisé !", "Email déjà utilisé", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }

        }
    }
}
