﻿namespace GestionBoxeMDI
{
    partial class frmInscription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInscription = new System.Windows.Forms.Label();
            this.lblInscNom = new System.Windows.Forms.Label();
            this.lblInscPrenom = new System.Windows.Forms.Label();
            this.lblInscTel = new System.Windows.Forms.Label();
            this.lblInscEmail = new System.Windows.Forms.Label();
            this.lblInscMDP = new System.Windows.Forms.Label();
            this.btnInscription = new System.Windows.Forms.Button();
            this.txtInscNom = new System.Windows.Forms.TextBox();
            this.txtInscPrenom = new System.Windows.Forms.TextBox();
            this.txtInscTel = new System.Windows.Forms.TextBox();
            this.txtInscEmail = new System.Windows.Forms.TextBox();
            this.txtInscMDP = new System.Windows.Forms.TextBox();
            this.txtInscConfirmerMDP = new System.Windows.Forms.TextBox();
            this.lblInscConfirmerMDP = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblInscription
            // 
            this.lblInscription.AutoSize = true;
            this.lblInscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscription.Location = new System.Drawing.Point(176, 12);
            this.lblInscription.Name = "lblInscription";
            this.lblInscription.Size = new System.Drawing.Size(72, 17);
            this.lblInscription.TabIndex = 0;
            this.lblInscription.Text = "Inscription";
            // 
            // lblInscNom
            // 
            this.lblInscNom.AutoSize = true;
            this.lblInscNom.Location = new System.Drawing.Point(127, 41);
            this.lblInscNom.Name = "lblInscNom";
            this.lblInscNom.Size = new System.Drawing.Size(29, 13);
            this.lblInscNom.TabIndex = 1;
            this.lblInscNom.Text = "&Nom";
            // 
            // lblInscPrenom
            // 
            this.lblInscPrenom.AutoSize = true;
            this.lblInscPrenom.Location = new System.Drawing.Point(113, 68);
            this.lblInscPrenom.Name = "lblInscPrenom";
            this.lblInscPrenom.Size = new System.Drawing.Size(43, 13);
            this.lblInscPrenom.TabIndex = 3;
            this.lblInscPrenom.Text = "&Prénom";
            // 
            // lblInscTel
            // 
            this.lblInscTel.AutoSize = true;
            this.lblInscTel.Location = new System.Drawing.Point(98, 95);
            this.lblInscTel.Name = "lblInscTel";
            this.lblInscTel.Size = new System.Drawing.Size(58, 13);
            this.lblInscTel.TabIndex = 5;
            this.lblInscTel.Text = "&Téléphone";
            // 
            // lblInscEmail
            // 
            this.lblInscEmail.AutoSize = true;
            this.lblInscEmail.Location = new System.Drawing.Point(124, 122);
            this.lblInscEmail.Name = "lblInscEmail";
            this.lblInscEmail.Size = new System.Drawing.Size(32, 13);
            this.lblInscEmail.TabIndex = 7;
            this.lblInscEmail.Text = "&Email";
            // 
            // lblInscMDP
            // 
            this.lblInscMDP.AutoSize = true;
            this.lblInscMDP.Location = new System.Drawing.Point(85, 149);
            this.lblInscMDP.Name = "lblInscMDP";
            this.lblInscMDP.Size = new System.Drawing.Size(71, 13);
            this.lblInscMDP.TabIndex = 9;
            this.lblInscMDP.Text = "&Mot de passe";
            // 
            // btnInscription
            // 
            this.btnInscription.Location = new System.Drawing.Point(162, 198);
            this.btnInscription.Name = "btnInscription";
            this.btnInscription.Size = new System.Drawing.Size(100, 23);
            this.btnInscription.TabIndex = 13;
            this.btnInscription.Text = "&Inscription";
            this.btnInscription.UseVisualStyleBackColor = true;
            this.btnInscription.Click += new System.EventHandler(this.btnInscription_Click);
            // 
            // txtInscNom
            // 
            this.txtInscNom.Location = new System.Drawing.Point(162, 37);
            this.txtInscNom.Name = "txtInscNom";
            this.txtInscNom.Size = new System.Drawing.Size(100, 20);
            this.txtInscNom.TabIndex = 2;
            // 
            // txtInscPrenom
            // 
            this.txtInscPrenom.Location = new System.Drawing.Point(162, 64);
            this.txtInscPrenom.Name = "txtInscPrenom";
            this.txtInscPrenom.Size = new System.Drawing.Size(100, 20);
            this.txtInscPrenom.TabIndex = 4;
            // 
            // txtInscTel
            // 
            this.txtInscTel.Location = new System.Drawing.Point(162, 91);
            this.txtInscTel.Name = "txtInscTel";
            this.txtInscTel.Size = new System.Drawing.Size(100, 20);
            this.txtInscTel.TabIndex = 6;
            // 
            // txtInscEmail
            // 
            this.txtInscEmail.Location = new System.Drawing.Point(162, 118);
            this.txtInscEmail.Name = "txtInscEmail";
            this.txtInscEmail.Size = new System.Drawing.Size(100, 20);
            this.txtInscEmail.TabIndex = 8;
            // 
            // txtInscMDP
            // 
            this.txtInscMDP.Location = new System.Drawing.Point(162, 145);
            this.txtInscMDP.Name = "txtInscMDP";
            this.txtInscMDP.Size = new System.Drawing.Size(100, 20);
            this.txtInscMDP.TabIndex = 10;
            this.txtInscMDP.UseSystemPasswordChar = true;
            // 
            // txtInscConfirmerMDP
            // 
            this.txtInscConfirmerMDP.Location = new System.Drawing.Point(162, 171);
            this.txtInscConfirmerMDP.Name = "txtInscConfirmerMDP";
            this.txtInscConfirmerMDP.Size = new System.Drawing.Size(100, 20);
            this.txtInscConfirmerMDP.TabIndex = 12;
            this.txtInscConfirmerMDP.UseSystemPasswordChar = true;
            // 
            // lblInscConfirmerMDP
            // 
            this.lblInscConfirmerMDP.AutoSize = true;
            this.lblInscConfirmerMDP.Location = new System.Drawing.Point(12, 175);
            this.lblInscConfirmerMDP.Name = "lblInscConfirmerMDP";
            this.lblInscConfirmerMDP.Size = new System.Drawing.Size(144, 13);
            this.lblInscConfirmerMDP.TabIndex = 11;
            this.lblInscConfirmerMDP.Text = "&Confirmer votre mot de passe";
            // 
            // frmInscription
            // 
            this.AcceptButton = this.btnInscription;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 235);
            this.Controls.Add(this.lblInscription);
            this.Controls.Add(this.lblInscNom);
            this.Controls.Add(this.lblInscPrenom);
            this.Controls.Add(this.lblInscTel);
            this.Controls.Add(this.lblInscEmail);
            this.Controls.Add(this.lblInscMDP);
            this.Controls.Add(this.lblInscConfirmerMDP);
            this.Controls.Add(this.txtInscNom);
            this.Controls.Add(this.txtInscPrenom);
            this.Controls.Add(this.txtInscTel);
            this.Controls.Add(this.txtInscEmail);
            this.Controls.Add(this.txtInscMDP);
            this.Controls.Add(this.txtInscConfirmerMDP);
            this.Controls.Add(this.btnInscription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmInscription";
            this.Text = "Inscription";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInscription;
        private System.Windows.Forms.Label lblInscNom;
        private System.Windows.Forms.Label lblInscPrenom;
        private System.Windows.Forms.Label lblInscTel;
        private System.Windows.Forms.Label lblInscEmail;
        private System.Windows.Forms.Label lblInscMDP;
        private System.Windows.Forms.Button btnInscription;
        private System.Windows.Forms.TextBox txtInscNom;
        private System.Windows.Forms.TextBox txtInscPrenom;
        private System.Windows.Forms.TextBox txtInscTel;
        private System.Windows.Forms.TextBox txtInscEmail;
        private System.Windows.Forms.TextBox txtInscMDP;
        private System.Windows.Forms.TextBox txtInscConfirmerMDP;
        private System.Windows.Forms.Label lblInscConfirmerMDP;
    }
}