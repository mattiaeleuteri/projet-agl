﻿namespace GestionBoxeMDI
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLogin = new System.Windows.Forms.Label();
            this.txtLoginEmail = new System.Windows.Forms.TextBox();
            this.txtLoginMDP = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblEmailLogin = new System.Windows.Forms.Label();
            this.lblLoginMDP = new System.Windows.Forms.Label();
            this.lklOubliMDP = new System.Windows.Forms.LinkLabel();
            this.lklInscription = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.Location = new System.Drawing.Point(136, 20);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(43, 17);
            this.lblLogin.TabIndex = 0;
            this.lblLogin.Text = "Login";
            // 
            // txtLoginEmail
            // 
            this.txtLoginEmail.Location = new System.Drawing.Point(107, 45);
            this.txtLoginEmail.Name = "txtLoginEmail";
            this.txtLoginEmail.Size = new System.Drawing.Size(100, 20);
            this.txtLoginEmail.TabIndex = 2;
            // 
            // txtLoginMDP
            // 
            this.txtLoginMDP.Location = new System.Drawing.Point(107, 72);
            this.txtLoginMDP.Name = "txtLoginMDP";
            this.txtLoginMDP.Size = new System.Drawing.Size(100, 20);
            this.txtLoginMDP.TabIndex = 4;
            this.txtLoginMDP.UseSystemPasswordChar = true;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(107, 98);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(100, 23);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "&Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblEmailLogin
            // 
            this.lblEmailLogin.AutoSize = true;
            this.lblEmailLogin.Location = new System.Drawing.Point(69, 49);
            this.lblEmailLogin.Name = "lblEmailLogin";
            this.lblEmailLogin.Size = new System.Drawing.Size(32, 13);
            this.lblEmailLogin.TabIndex = 1;
            this.lblEmailLogin.Text = "&Email";
            // 
            // lblLoginMDP
            // 
            this.lblLoginMDP.AutoSize = true;
            this.lblLoginMDP.Location = new System.Drawing.Point(30, 76);
            this.lblLoginMDP.Name = "lblLoginMDP";
            this.lblLoginMDP.Size = new System.Drawing.Size(71, 13);
            this.lblLoginMDP.TabIndex = 3;
            this.lblLoginMDP.Text = "&Mot de passe";
            // 
            // lklOubliMDP
            // 
            this.lklOubliMDP.AutoSize = true;
            this.lklOubliMDP.Location = new System.Drawing.Point(106, 133);
            this.lklOubliMDP.Name = "lklOubliMDP";
            this.lklOubliMDP.Size = new System.Drawing.Size(102, 13);
            this.lklOubliMDP.TabIndex = 6;
            this.lklOubliMDP.TabStop = true;
            this.lklOubliMDP.Text = "Mot de passe &oublié";
            this.lklOubliMDP.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lklOubliMDP_LinkClicked);
            // 
            // lklInscription
            // 
            this.lklInscription.AutoSize = true;
            this.lklInscription.Location = new System.Drawing.Point(64, 183);
            this.lklInscription.Name = "lklInscription";
            this.lklInscription.Size = new System.Drawing.Size(187, 13);
            this.lklInscription.TabIndex = 7;
            this.lklInscription.TabStop = true;
            this.lklInscription.Text = "&Pas encore membre ? Inscrivez-vous !";
            this.lklInscription.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lklInscription_LinkClicked);
            // 
            // frmLogin
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(306, 215);
            this.ControlBox = false;
            this.Controls.Add(this.lblLogin);
            this.Controls.Add(this.lblEmailLogin);
            this.Controls.Add(this.lblLoginMDP);
            this.Controls.Add(this.lklInscription);
            this.Controls.Add(this.lklOubliMDP);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtLoginEmail);
            this.Controls.Add(this.txtLoginMDP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmLogin";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.TextBox txtLoginEmail;
        private System.Windows.Forms.TextBox txtLoginMDP;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblEmailLogin;
        private System.Windows.Forms.Label lblLoginMDP;
        private System.Windows.Forms.LinkLabel lklOubliMDP;
        private System.Windows.Forms.LinkLabel lklInscription;
    }
}