﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmLogin : Form
    {
        frmInscription frmInscription = null;
        frmOubliMDP frmOubliMDP = null;


        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            OracleConnection conn = new OracleConnection("DATA SOURCE=localhost;PERSIST SECURITY INFO=True;USER ID=GestionBoxe_data;PASSWORD=GestionBoxe_data;");
            try
            {
                if (txtLoginEmail.Text != "" && txtLoginMDP.Text != "")
                {
                    conn.Open();
                    OracleCommand command = new OracleCommand("SELECT * FROM UTILISATEUR WHERE UTIL_EMAIL = '" + txtLoginEmail.Text + "' AND UTIL_MDP = '" + txtLoginMDP.Text + "'", conn);
                    OracleDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        int parse;
                        if (reader.IsDBNull(1))
                        {
                            parse = -1;

                        }
                        else
                        {
                            parse = reader.GetInt16(1);
                        }
                        Session.RemplirSession(reader.GetInt16(0), parse, reader.GetInt16(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6));
                        this.Close();
                        reader.Close();
                    }
                    else
                    {
                        MessageBox.Show("Utilisateur introuvable, veuillez rentrer les bonnes données de connexion", "Utilisateur introuvable", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    }


                }
                else
                {
                    MessageBox.Show("Login incomplet, veuillez remplir les données de connexion", "Login incomplet", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            if (frmInscription != null) frmInscription.Close();
            if (frmOubliMDP != null) frmOubliMDP.Close();
        }

        private void lklOubliMDP_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (frmOubliMDP == null || frmOubliMDP.IsDisposed)
            {
                frmOubliMDP = new frmOubliMDP();
            }
            frmOubliMDP.Show();
            frmOubliMDP.Focus();
        }

        private void lklInscription_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (frmInscription == null || frmInscription.IsDisposed)
            {
                frmInscription = new frmInscription();
            }
            frmInscription.Show();
            frmInscription.Focus();
        }
    }
}
