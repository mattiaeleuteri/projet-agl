﻿using System;
using System.Windows.Forms;

namespace GestionBoxeMDI
{
    public partial class frmOubliMDP : Form
    {
        public frmOubliMDP()
        {
            InitializeComponent();
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
