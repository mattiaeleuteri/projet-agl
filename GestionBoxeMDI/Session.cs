﻿using System.Runtime.CompilerServices;
using System.Security.Principal;

public static class Session
{
    public static int utilId;
    public static int aboId;
    public static int droitId;
    public static string nom;
    public static string prenom;
    public static string tel;
    public static string email;

    public static void RemplirSession(int utilId, int aboId, int droitId, string nom, string prenom, string tel, string email)
    {
        Session.utilId = utilId;
        Session.aboId = aboId;
        Session.droitId = droitId;
        Session.nom = nom;
        Session.prenom = prenom;
        Session.tel = tel;
        Session.email = email;
    }
}