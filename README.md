Projet AGL pour le cours du semestre 4 de la HEG 2020
#############################################
Logiciel de gestion d'une salle de boxe

#############################################
Comptes pour tester les droits :

admin:admin
coach:coach
membre:membre

#############################################
Version finale de mon projet.
Login et Inscription fonctionnent normalement sans bug.
Le lien "J'ai oublié mon mot de passe" n'est pas implanté car par de serveur mail.
Utiliser un des trois comptes mis à disposition pour essayer l'accès avec des droits différents.

La fenêtre principale propose au maximum 4 fonctionnalités :

- Liste des cours
- Liste des abonnements
- Liste des factures
- Liste des membres (accessible uniquement via un compte avec les droits d'administrateur (admin:admin))

#############################################
Liste des cours
Divers filtres sont mis en place.
La liste permet de voir tous les cours, d'en rechercher et de les filtrer selon nos inscriptions.

L'admin et le coach peuvent accéder à un bouton pour ajouter un cours à la liste.

#############################################
Liste des abonnements
Filtre de recherche par texte.
La liste permet de voir tous les abonnements si l'utilisateur est un coach ou un admin. Sinon les membres peuvent consulter leur abonnement en cours ou voir les abonnements auxquels ils pourraient souscrire.

L'admin peut accéder à un bouton pour ajouter un abonnement à la liste.

#############################################
Liste des factures
Divers filtres sont mis en place.
La liste permet de voir toutes les factures, d'en rechercher et de les filtrer selon leur statut.

#############################################
Liste des membres
Divers filtres sont mis en place.
La liste permet de voir tous les membres, d'en rechercher et de les filtrer selon leur activité (abonnement ou non).

L'admin peut ajouter un membre à partir d'un bouton.

#############################################
